using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	[System.Serializable]
	public class SearchyCore : ScriptableObject
	{
		public static SearchyCore instance = null;

		public static string Version
		{
			get
			{
				return "1.0";
			}
		}

		public class SearchySourceType
		{
			public string typeFullName;
			public Type type;
		}
		
		private List<SearchySourceType> _sourceTypes = null;
		private void InitSourceTypes()
		{
			_sourceTypes = new List<SearchySourceType>();

			foreach(Assembly cAssembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if(!cAssembly.FullName.Contains("Editor"))
				{
					continue;
				}

				foreach(Type cType in cAssembly.GetTypes())
				{
					if(cType == typeof(SearchySourceBase))
					{
						continue;
					}

					if(!typeof(SearchySourceBase).IsAssignableFrom(cType))
					{
						continue;
					}

					SearchySourceType nSourceType = new SearchySourceType() { typeFullName = cType.FullName, type = cType };
					_sourceTypes.Add(nSourceType);
				}
			}
		}

		public List<SearchySourceType> SourceTypes
		{
			get
			{
				if(_sourceTypes == null)
				{
					InitSourceTypes();
				}

				return _sourceTypes;
			}
		}

		public class SearchyEntryType
		{
			public string typeFullName;
			public Type type;
		}
		
		private List<SearchyEntryType> _entryTypes = null;
		private void InitEntryTypes()
		{
			_entryTypes = new List<SearchyEntryType>();

			foreach(Assembly cAssembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if(!cAssembly.FullName.Contains("Editor"))
				{
					continue;
				}

				foreach(Type cType in cAssembly.GetTypes())
				{
					if(cType == typeof(SearchyEntryBase))
					{
						continue;
					}

					if(!typeof(SearchyEntryBase).IsAssignableFrom(cType))
					{
						continue;
					}

					SearchyEntryType nEntryType = new SearchyEntryType() { typeFullName = cType.FullName, type = cType };
					_entryTypes.Add(nEntryType);
				}
			}
		}

		public List<SearchyEntryType> EntryTypes
		{
			get
			{
				if(_entryTypes == null)
				{
					InitEntryTypes();
				}

				return _entryTypes;
			}
		}
		
		public void MarkAllNeedsFullRefresh(Type inType = null)
		{
			_filteredEntriesNeedRefresh = true;
			
			foreach(var cRuntimeSource in SourceChain)
			{
				if(inType == null || cRuntimeSource.SourceType == inType)
				{
					cRuntimeSource.MarkNeedsFullRefresh();
				}
			}

			foreach(var cSourceInstance in AllActiveSourceInstances)
			{
				if(inType == null || cSourceInstance.GetType() == inType)
				{
					cSourceInstance.MarkNeedsFullRefresh();
				}
			}
		}

		private Dictionary<Type, SearchySourceBase> _sourceInstanceCache = new Dictionary<Type, SearchySourceBase>();
		private List<SearchySourceBase> _sourceInstancesUnique = new List<SearchySourceBase>();
		
		public SearchySourceBase GetSourceInstanceWithTypeName(string inTypeFullName, bool inUseCache = true)
		{
			foreach(var cSourceType in SourceTypes)
			{
				if(cSourceType.typeFullName == inTypeFullName)
				{
					return GetSourceInstanceWithType(cSourceType.type);
				}
			}

			return null;
		}

		public SearchySourceBase GetSourceInstanceWithType(Type inType, bool inUseCache = true)
		{
			foreach(var cSourceType in SourceTypes)
			{
				if(cSourceType.type == inType)
				{
					if(inUseCache)
					{
						if(_sourceInstanceCache.ContainsKey(inType))
						{
							return _sourceInstanceCache[inType];
						}
					}

					SearchySourceBase nSourceInstance = Activator.CreateInstance(inType) as SearchySourceBase;

					nSourceInstance.InternalInit();

					if(inUseCache)
					{
						_sourceInstanceCache.Add(inType, nSourceInstance);
					}
					else
					{
						_sourceInstancesUnique.Add(nSourceInstance);
					}

					return nSourceInstance;
				}
			}

			return null;
		}

		[NonSerialized]
		private static bool _inited = false;
		public static void Init()
		{
			if(_inited)
			{
				return;
			}

			if(instance == null)
			{
				instance = ScriptableObject.CreateInstance<SearchyCore>();
				instance.hideFlags = HideFlags.HideAndDontSave;
			}

			#if UNITY_2017
				EditorApplication.projectWindowChanged -= instance.OnProjectChange;
				EditorApplication.hierarchyWindowChanged -= instance.OnHierarchyChange;
				EditorApplication.projectWindowChanged += instance.OnProjectChange;
				EditorApplication.hierarchyWindowChanged += instance.OnHierarchyChange;
			#else
				EditorApplication.projectChanged -= instance.OnProjectChange;
				EditorApplication.hierarchyChanged -= instance.OnHierarchyChange;
				EditorApplication.projectChanged += instance.OnProjectChange;
				EditorApplication.hierarchyChanged += instance.OnHierarchyChange;
			#endif
			
			instance.InitSourceTypes();

			if(instance.SourceChain.Count == 0)
			{
				SearchySource_Main nMainSource = instance.GetSourceInstanceWithType(typeof(SearchySource_Main)) as SearchySource_Main;
				instance.AddSourceToChain(nMainSource);
			}

			foreach(SearchyRuntimeSource cRuntimeSource in instance.SourceChain)
			{
				if(cRuntimeSource.SourceInstance == null)
				{
					continue;
				}
				cRuntimeSource.SourceInstance.InternalInit();
			}

			if(SearchyWindow.instance != null)
			{
				SearchyWindow.instance.ScrollToSelectedEntry();
			}

			_inited = true;
		}

		private void OnEnable()
		{
			instance = this;
			Init();
		}

		private List<SearchyRuntimeSource> _sourceChain = new List<SearchyRuntimeSource>();
		public List<SearchyRuntimeSource> SourceChain
		{
			get
			{
				return _sourceChain;
			}
		}

		public SearchyRuntimeSource AddSourceToChain(Type inType, bool inUseCache = true)
		{
			SearchyRuntimeSource nSourceChainEntry = new SearchyRuntimeSource(inType, inUseCache);
			SourceChain.Add(nSourceChainEntry);

			if(CurrentSourceInChain != null)
			{
				CurrentSourceInChain.serializedSearchText = SearchText;
			}
			
			SearchText = "";
			
			Refresh();

			return nSourceChainEntry;
		}
		
		public SearchyRuntimeSource AddSourceToChain(SearchySourceBase inSourceInstance)
		{
			SearchyRuntimeSource nSourceChainEntry = new SearchyRuntimeSource(inSourceInstance);
			SourceChain.Add(nSourceChainEntry);

			if(CurrentSourceInChain != null)
			{
				CurrentSourceInChain.serializedSearchText = SearchText;
			}
			SearchText = "";
			
			Refresh();

			return nSourceChainEntry;
		}

		public void RemoveSourceFromChain()
		{
			string lastSearchText = CurrentSourceInChain.serializedSearchText;
			CurrentSourceInChain.SourceInstance.InternalSaveConfig();

			if(_sourceInstancesUnique.Contains(CurrentSourceInChain.SourceInstance))
			{
				_sourceInstancesUnique.Remove(CurrentSourceInChain.SourceInstance);
			}

			SourceChain.RemoveAt(SourceChain.Count-1);
			if(!string.IsNullOrEmpty(lastSearchText))
			{
				SearchText = lastSearchText;
			}
			else
			{
				SearchText = "";
			}
			Refresh();
		}

		public void ClearSourceChain()
		{
			while(SourceChain.Count > 1)
			{
				RemoveSourceFromChain();
			}

			SearchText = "";
		}

		public SearchyRuntimeSource MainSourceInChain
		{
			get
			{
				return SourceChain[0];
			}
		}

		public SearchyRuntimeSource CurrentSourceInChain
		{
			get
			{
				return SourceChain[SourceChain.Count-1];
			}
		}

		public SearchyRuntimeSource PreviousSourceInChain
		{
			get
			{
				if(SourceChain.Count <= 1)
				{
					return null;
				}

				return SourceChain[SourceChain.Count-2];
			}
		}

		public void Refresh()
		{
			_filteredEntriesNeedRefresh = true;
		}

		[NonSerialized]
		private SearchyEntryBase _lastSelectedEntry = null;
		public SearchyEntryBase SelectedEntry
		{
			get
			{
				if(FilteredEntries.Count == 0)
				{
					return null;
				}

				SearchyEntryBase cSelected = null;

				if(CurrentSourceInChain.SelectedEntry != null)
				{
					cSelected = CurrentSourceInChain.SelectedEntry;
				}
				else
				{
					CurrentSourceInChain.SelectedEntry = FilteredEntries[0];
					cSelected = FilteredEntries[0];
				}

				if(cSelected != _lastSelectedEntry)
				{
					_lastSelectedEntry = cSelected;
					if(SearchyWindow.instance != null)
					{
						SearchyWindow.instance.ScrollToSelectedEntry();
					}
				}
				return cSelected;
			}

			set
			{
				CurrentSourceInChain.SelectedEntry = value;
			}
		}

		public List<SearchyEntryBase> GetAllEntriesForCurrentMode()
		{
			return CurrentSourceInChain.SourceInstance.InternalGetEntries( (string.IsNullOrEmpty(SearchText) ? SearchyGetEntryMode.NoSearch : SearchyGetEntryMode.Search) );
		}

		public SearchyEntryBase GetEntryFromCurrentFilterWithHash(string inHash)
		{
			List<SearchyEntryBase> filteredEntries = FilteredEntries;
			foreach(var cEntry in filteredEntries)
			{
				if(cEntry.Hash == inHash)
				{
					return cEntry;
				}
			}

			return null;
		}

		[NonSerialized]
		public bool filteredEntryRectsNeedRefresh = true;
		[NonSerialized]
		private bool _filteredEntriesNeedRefresh = true;
		[NonSerialized]
		private List<SearchyEntryBase> _filteredEntries = new List<SearchyEntryBase>();
		public List<SearchyEntryBase> FilteredEntries
		{
			get
			{
				if(_filteredEntriesNeedRefresh)
				{
					_filteredEntriesNeedRefresh = false;
					_filteredEntries.Clear();
					filteredEntryRectsNeedRefresh = true;

					CurrentSourceInChain.ClearSelectedEntryInstance();
					List<SearchyEntryBase> cEntries = GetAllEntriesForCurrentMode();
					if(string.IsNullOrEmpty(SearchText) || SearchText == "*")
					{
						_filteredEntries.AddRange(cEntries);
						
						int topIndex = 0;
						for(int i = 0; i < _filteredEntries.Count; i++)
						{
							if(!FilteredEntries[i].IsVisible)
							{
								_filteredEntries.RemoveAt(i);
								i--;
								continue;
							}

							if(_filteredEntries[i].parentSource == null)
							{
								continue;
							}

							if(_filteredEntries[i].parentSource.MatchFavorite(_filteredEntries[i]))
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}

						}
					}
					else
					{
						foreach(SearchyEntryBase cEntry in cEntries)
						{
							if(!cEntry.IsVisible)
							{
								continue;
							}

							bool match = true;
							foreach(string cString in _searchTextSplit)
							{
								if(!cEntry.SearchText.Contains(cString))
								{
									match = false;
									break;
								}
							}

							if(match)
							{
								_filteredEntries.Add(cEntry);
							}
						}

						int topIndex = 0;

						for(int i = 0; i < _filteredEntries.Count; i++)
						{
							if(_filteredEntries[i].parentSource == null)
							{
								continue;
							}

							if(_filteredEntries[i].parentSource.MatchFavorite(_filteredEntries[i]))
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}
						}

						for(int i = topIndex; i < _filteredEntries.Count; i++)
						{
							if(_filteredEntries[i].DisplayNameLower.IndexOf(_searchTextSplit[0]) == 0)
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}
						}

						for(int i = topIndex; i < _filteredEntries.Count; i++)
						{
							if(_filteredEntries[i].DisplayNameLower.IndexOf(_searchTextSplit[0]) == 0 && _filteredEntries[i].SubLabel != null &&_filteredEntries[i].SubLabelLower.Contains(_searchTextSplit[0]))
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}
						}

						for(int i = topIndex; i < _filteredEntries.Count; i++)
						{
							if(_filteredEntries[i].DisplayNameLower.Contains(_searchTextSplit[0]) && _filteredEntries[i].SubLabel != null && _filteredEntries[i].SubLabelLower.Contains(_searchTextSplit[0]))
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}
						}

						for(int i = topIndex; i < _filteredEntries.Count; i++)
						{
							if(_filteredEntries[i].DisplayNameLower.Contains(_searchTextSplit[0]))
							{
								_filteredEntries.Insert(topIndex, _filteredEntries[i]);
								_filteredEntries.RemoveAt(i+1);
								topIndex++;
							}
						}
					}

					if(SearchyWindow.instance != null)
					{
						SearchyWindow.instance.ScrollToSelectedEntry();
					}
				}
				return _filteredEntries;
			}
		}

		public List<SearchySourceBase> AllActiveSourceInstances
		{
			get
			{
				List<SearchySourceBase> ret = new List<SearchySourceBase>();
				ret.AddRange(_sourceInstanceCache.Values);
				ret.AddRange(_sourceInstancesUnique);
				return ret;
			}
		}

		private void OnProjectChange()
		{
			foreach(var cSourceInstance in AllActiveSourceInstances)
			{
				if(!cSourceInstance.RefreshOnProjectChange)
				{
					continue;
				}

				cSourceInstance.MarkNeedsFullRefresh();
			}

			Refresh();
		}

		private void OnHierarchyChange()
		{
			foreach(var cSourceInstance in AllActiveSourceInstances)
			{
				if(!cSourceInstance.RefreshOnHierarchyChange)
				{
					continue;
				}

				cSourceInstance.MarkNeedsFullRefresh();
			}

			Refresh();
		}

		private bool _searchTextSet = false;
		private string _searchText = "";
		private string[] _searchTextSplit = null;
		public string SearchText
		{
			set
			{
				if(_searchText != value || !_searchTextSet)
				{
					_searchText = value;
					_searchTextSet = true;
					_searchTextSplit = value.ToLower().Split(new char[] { ' ' });
					_filteredEntriesNeedRefresh = true;
					CurrentSourceInChain.ClearSelectedEntry();
				}
			}
			get
			{
				return _searchText;
			}
		}
	}
}
