using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	[System.Serializable]
	public class SearchyRuntimeSource : ISerializationCallbackReceiver
	{
		public SearchyRuntimeSource()
		{
		}

		public SearchyRuntimeSource(SearchySourceBase inSourceInstance)
		{
			if(inSourceInstance == null)
			{
				Debug.LogError("Attempted to create runtime source with null source instance");
				return;
			}

			SourceType = inSourceInstance.GetType();

			_sourceInstance = SearchyCore.instance.GetSourceInstanceWithType(SourceType);
			_sourceInstance.boundRuntimeSource = this;
		}

		public SearchyRuntimeSource(Type inType, bool inUseSourceCache = true)
		{
			useSourceCache = inUseSourceCache;
			SourceType = inType;
		}

		public bool enabled = true;

		[SerializeField]
		public bool useSourceCache;

		[SerializeField]
		public string serializedSearchText;
		[SerializeField]
		public Vector2 serializedScrollPos;
		
		[SerializeField]
		private string _serializedSelectedEntryHash;

		[SerializeField]
		private List<string> _serializedOptionalStringDataKeys = new List<string>();
		[SerializeField]
		private List<string> _serializedOptionalStringDataValues = new List<string>();
		private Dictionary<string, string> _serializedOptionalStringDataDict = new Dictionary<string, string>();
		
		[SerializeField]
		private List<string> _serializedOptionalIntDataKeys = new List<string>();
		[SerializeField]
		private List<int> _serializedOptionalIntDataValues = new List<int>();
		private Dictionary<string, int> _serializedOptionalIntDataDict = new Dictionary<string, int>();

		public void MarkNeedsFullRefresh()
		{
			ClearSelectedEntryInstance();
			SourceInstance.MarkNeedsFullRefresh();
		}

		public void ClearSelectedEntryInstance()
		{
			_selectedEntry = null;
		}

		public void ClearSelectedEntry()
		{
			SelectedEntry = null;
		}

		private SearchyEntryBase _selectedEntry;
		public SearchyEntryBase SelectedEntry
		{
			get
			{
				if(_selectedEntry == null && !string.IsNullOrEmpty(_serializedSelectedEntryHash))
				{
					_selectedEntry = SourceInstance.FindEntryWithHash(_serializedSelectedEntryHash); //SearchyCore.instance.GetEntryFromCurrentFilterWithHash(_serializedSelectedEntryHash);
				}
				
				return _selectedEntry;
			}

			set
			{
				_selectedEntry = value;
				if(value != null)
				{
					_serializedSelectedEntryHash = value.Hash;
				}
				else
				{
					_serializedSelectedEntryHash = null;
				}
			}
		}

		public void OnBeforeSerialize()
		{
			_serializedOptionalStringDataKeys.Clear();
			_serializedOptionalStringDataValues.Clear();
			foreach(var cKVP in _serializedOptionalStringDataDict)
			{
				_serializedOptionalStringDataKeys.Add(cKVP.Key);
				_serializedOptionalStringDataValues.Add(cKVP.Value);
			}

			_serializedOptionalIntDataKeys.Clear();
			_serializedOptionalIntDataValues.Clear();
			foreach(var cKVP in _serializedOptionalIntDataDict)
			{
				_serializedOptionalIntDataKeys.Add(cKVP.Key);
				_serializedOptionalIntDataValues.Add(cKVP.Value);
			}

			if(SelectedEntry != null)
			{
				_serializedSelectedEntryHash = SelectedEntry.Hash;
			}
		}

		public void OnAfterDeserialize()
		{
			_serializedOptionalStringDataDict = new Dictionary<string, string>();
			
			if(_serializedOptionalStringDataKeys.Count == _serializedOptionalStringDataValues.Count)
			{
				for(int i = 0; i < _serializedOptionalStringDataKeys.Count; i++)
				{
					_serializedOptionalStringDataDict[_serializedOptionalStringDataKeys[i]] = _serializedOptionalStringDataValues[i];
				}
			} 

			_serializedOptionalIntDataDict = new Dictionary<string, int>();
			
			if(_serializedOptionalIntDataKeys.Count == _serializedOptionalIntDataValues.Count)
			{
				for(int i = 0; i < _serializedOptionalIntDataKeys.Count; i++)
				{
					_serializedOptionalIntDataDict[_serializedOptionalIntDataKeys[i]] = _serializedOptionalIntDataValues[i];
				}
			}
		}

		public string GetOptionalData(string inKey, string inDefaultValue)
		{
			if(_serializedOptionalStringDataDict.ContainsKey(inKey))
			{
				return _serializedOptionalStringDataDict[inKey];
			}

			SetOptionalData(inKey, inDefaultValue);
			return inDefaultValue;
		}

		public void SetOptionalData(string inKey, string inValue)
		{
			_serializedOptionalStringDataDict[inKey] = inValue;
		}

		public int GetOptionalData(string inKey, int inDefaultValue)
		{
			if(_serializedOptionalIntDataDict.ContainsKey(inKey))
			{
				return _serializedOptionalIntDataDict[inKey];
			}

			SetOptionalData(inKey, inDefaultValue);
			return inDefaultValue;
		}

		public void SetOptionalData(string inKey, int inValue)
		{
			_serializedOptionalIntDataDict[inKey] = inValue;
		}
		
		private Type _sourceType;
		public Type SourceType
		{
			private set
			{
				_sourceType = value;
				_serializedSourceTypeAssemblyFullName = value.Assembly.FullName;
				_serializedSourceTypeFullName = value.FullName;
			}

			get
			{
				if(_sourceType == null)
				{
					if(string.IsNullOrEmpty(_serializedSourceTypeAssemblyFullName))
					{
						return null;
					}

					if(string.IsNullOrEmpty(_serializedSourceTypeFullName))
					{
						return null;
					}

					Assembly sourceTypeAssembly = Assembly.Load(_serializedSourceTypeAssemblyFullName);
					
					if(sourceTypeAssembly != null)
					{
						_sourceType = sourceTypeAssembly.GetType(_serializedSourceTypeFullName);
					}

					if(_sourceType == null)
					{
						Debug.LogError("Searchy: Unable to find Type " + _serializedSourceTypeFullName);
					}
				}
				return _sourceType;
			}
		}

		[SerializeField]
		private string _serializedSourceTypeAssemblyFullName;
		[SerializeField]
		private string _serializedSourceTypeFullName;
		
		private SearchySourceBase _sourceInstance;
		public SearchySourceBase SourceInstance
		{
			get
			{
				if(SourceType == null)
				{
					Debug.LogError("Searchy: Unable to find type " + _serializedSourceTypeFullName);
					return null;
				}

				if(_sourceInstance == null && SourceType != null)
				{
					_sourceInstance = SearchyCore.instance.GetSourceInstanceWithType(SourceType, useSourceCache);
				}

				// Bind source instance to this run time source
				_sourceInstance.boundRuntimeSource = this;

				return _sourceInstance;
			}
		}
	}
}
