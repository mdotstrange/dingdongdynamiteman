using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Material : SearchyEntryBaseAsset
	{
		[NonSerialized]
		public Editor editor;

		public override bool HasPreview
		{
			get
			{
				return true;
			}
		}

		public override void DrawPreview(Rect inRect)
		{
			if(editor == null)
			{
				Material asset = AssetDatabase.LoadAssetAtPath(AssetPath, typeof(Material)) as Material;
				if(asset != null)
				{
					editor = Editor.CreateEditor(asset);
				}
			}

			if(editor != null)
			{
				editor.OnInteractivePreviewGUI(inRect, EditorStyles.helpBox);
			}
		}

		public override void FinishPreview()
		{
			if(editor != null)
			{
				UnityEngine.Object.DestroyImmediate(editor);
			}
		}
	}
}
