using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Scene : SearchyEntryBaseAsset
	{
		public override bool IsOpenable
		{
			get
			{
				return true;
			}
		}

		public override void Command_Open()
		{
			base.Command_Open();
			
			SearchyWindow.CloseWindow();
			if(EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
			{
				EditorSceneManager.OpenScene(AssetPath);
			}
		}
	}
}
