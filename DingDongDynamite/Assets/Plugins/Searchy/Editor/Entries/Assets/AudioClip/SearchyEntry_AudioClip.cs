using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_AudioClip : SearchyEntryBaseAsset
	{
		protected bool _playedSound;

		public override void RefreshCommandList()
		{
			AddListCommand("Play", "Play sound", Command_Play);
			AddListCommand("Select", "Select sound in project view", Command_Select);
		}

		public override void CleanUp()
		{
			if(_playedSound)
			{
				StopAllSounds();
			}
		}

		private void PlaySound(object inObj)
		{
			_playedSound = true;

			Assembly assemblyUnityEditor = typeof(AudioImporter).Assembly;
			Type typeAudioUtil = assemblyUnityEditor.GetType("UnityEditor.AudioUtil");
			MethodInfo methodPlayClip = typeAudioUtil.GetMethod("PlayClip", BindingFlags.Static | BindingFlags.Public, null, new System.Type[] { typeof(AudioClip) }, null);
			methodPlayClip.Invoke(null,new object[] { inObj });
		}

		private void StopAllSounds()
		{
			Assembly assemblyUnityEditor = typeof(AudioImporter).Assembly;
			Type typeAudioUtil = assemblyUnityEditor.GetType("UnityEditor.AudioUtil");
			MethodInfo methodStopAllClips = typeAudioUtil.GetMethod("StopAllClips", BindingFlags.Static | BindingFlags.Public, null, new System.Type[]{}, null);
			methodStopAllClips.Invoke(null, new object[] {});
		}

		private void Command_Play()
		{
			StopAllSounds();

			object audioObj = AssetDatabase.LoadAssetAtPath(AssetPath, typeof(UnityEngine.Object));
			if(audioObj == null)
			{
				return;
			}
			
			PlaySound(audioObj);
		}
	}
}
