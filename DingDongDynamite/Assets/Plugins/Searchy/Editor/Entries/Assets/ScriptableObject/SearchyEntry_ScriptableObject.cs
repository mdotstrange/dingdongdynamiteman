using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_ScriptableObject : SearchyEntryBaseAsset
	{
		public override void RefreshCommandList()
		{
			AddListCommand("Select", "Select object in project view", Command_Select);
		}
	}
}
