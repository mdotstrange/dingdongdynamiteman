using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	public class SearchyEntryBaseAsset : SearchyEntryBase
	{
		public string guid;

		protected string _assetPath = null;
		public string AssetPath
		{
			get
			{
				if(_assetPath == null)
				{
					_assetPath = AssetDatabase.GUIDToAssetPath(guid);
				}

				return _assetPath;
			}
		}

		protected string _assetPathFolder = null;
		public string AssetPathFolder
		{
			get
			{
				if(_assetPathFolder == null)
				{
					_assetPathFolder = (System.IO.Path.GetDirectoryName(AssetPath.Substring(6)) + "/").Replace("\\", "/").Replace("//", "/");
				}

				return _assetPathFolder;
			}
		}

		public override bool IsValid
		{
			get
			{
				return !string.IsNullOrEmpty(AssetPath);
			}
		}

		public override string Hash
		{
			get
			{
				return guid;
			}
			set
			{
				guid = value;
			}
		}

		public override Texture2D Icon
		{
			get
			{
				Texture2D tex = AssetDatabase.GetCachedIcon(AssetPath) as Texture2D;
				if(tex == null)
				{
					return SearchyWindowUtils.LoadEditorIcon("Prefab Icon");
				}
				return tex;
			}
		}

		public override string DisplayName
		{
			get
			{
				if(_displayName == null)
				{
					if(!IsValid)
					{
						_displayName = "File missing";
					}
					else
					{
						_displayName = System.IO.Path.GetFileName(AssetPath);
					}
				}
				
				return _displayName;
			}
		}

		public override string SubLabel
		{
			get
			{
				if(_subLabel == null && IsValid)
				{
					_subLabel = AssetPathFolder;
				}
				
				return _subLabel;
			}
		}

		public virtual bool IsSelectable
		{
			get
			{
				return true;
			}
		}

		public virtual bool IsOpenable
		{
			get
			{
				return false;
			}
		}

		public override bool AddDefaultTabCommand
		{
			get
			{
				return true;
			}
		}

		public override bool AddDefaultFavoriteCommand
		{
			get
			{
				return true;
			}
		}

		public override void RefreshCommandList()
		{
			if(IsOpenable)
			{
				AddListCommand("Open", "Open", Command_Open);
			}

			if(IsSelectable)
			{
				AddListCommand("Select", "Select in project view", Command_Select);
			}
		}

		public virtual void Command_Open()
		{
			if(parentSource != null)
			{
				parentSource.AddRecent(this);
			}
		}

		protected void Command_Select()
		{
			SearchyWindow.CloseWindow();

			if(parentSource != null)
			{
				parentSource.AddRecent(this);
			}

			Selection.activeObject = AssetDatabase.LoadAssetAtPath(AssetPath, typeof(UnityEngine.Object));

			System.Type typeProjectWindow = typeof(EditorWindow).Assembly.GetType("UnityEditor.ProjectBrowser");
			EditorWindow projectWindow = EditorWindow.GetWindow(typeProjectWindow);
			MethodInfo methodFrameObject = typeProjectWindow.GetMethod("FrameObject");
			methodFrameObject.Invoke(projectWindow, new object[] { Selection.activeInstanceID, false } );
		}
	}
}
