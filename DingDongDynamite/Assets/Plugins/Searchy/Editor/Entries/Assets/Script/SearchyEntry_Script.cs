using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Script : SearchyEntryBaseAsset
	{
		public override bool IsOpenable
		{
			get
			{
				return true;
			}
		}

		public override void Command_Open()
		{
			base.Command_Open();
			
			SearchyWindow.CloseWindow();
			UnityEngine.Object scriptObj = (UnityEngine.Object)AssetDatabase.LoadAssetAtPath(AssetPath, typeof(TextAsset));
			if(scriptObj != null)
			{
				AssetDatabase.OpenAsset(scriptObj);
			}
		}
	}
}
