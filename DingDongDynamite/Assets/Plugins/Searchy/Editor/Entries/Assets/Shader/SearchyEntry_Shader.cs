using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Shader : SearchyEntryBaseAsset
	{
		public override bool IsOpenable
		{
			get
			{
				return true;
			}
		}

		public override void Command_Open()
		{
			base.Command_Open();
			
			SearchyWindow.CloseWindow();
			UnityEngine.Object shaderObj = (UnityEngine.Object)AssetDatabase.LoadAssetAtPath(AssetPath, typeof(UnityEngine.Object));
			if(shaderObj != null)
			{
				AssetDatabase.OpenAsset(shaderObj);
			}
		}
	}
}
