using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Command : SearchyEntryBase
	{
		public SearchyCommand parentCommand;
		
		public override Texture2D Icon
		{
			get
			{
				return parentCommand.parentEntry.Icon;
			}
		}

		public override string DisplayName
		{
			get
			{
				return parentCommand.DisplayLabel;
			}
		}

		public override string SubLabel
		{
			get
			{
				return parentCommand.TipLabel;
			}
		}

		public override void RefreshCommands()
		{
			AddPrimaryCommand("Run", Run_Command);
			AddRightCommand("Run", Run_Command);
		}

		private void Run_Command()
		{
			if(parentCommand == null || parentCommand.parentEntry == null)
			{
				return;
			}

			SearchyCore.instance.RemoveSourceFromChain();
			parentCommand.Activate();
		}
	}
}
