using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	[System.Serializable]
	public class SearchyCommandEvent : UnityEvent {}

	[System.Serializable]
	public class SearchyCommand
	{
		[NonSerialized]
		public SearchyEntryBase parentEntry;
		
		public SearchyCommandEvent activateEvent;

		[SerializeField]
		protected string _displayLabel = "No label set";
		public string DisplayLabel
		{
			get
			{
				return _displayLabel;
			}
			set
			{
				_displayLabel = value;
			}
		}

		[SerializeField]
		protected string _tipLabel = null;
		public string TipLabel
		{
			get
			{
				return _tipLabel;
			}
			set
			{
				_tipLabel = value;
			}
		}

		public bool Activate()
		{
			if(activateEvent == null)
			{
				Debug.LogError("No event");
				return false;
			}

			activateEvent.Invoke();

			return true;
		}
	}
}
