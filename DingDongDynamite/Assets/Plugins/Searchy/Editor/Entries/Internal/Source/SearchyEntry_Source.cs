using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Source : SearchyEntryBase
	{
		public string serializedSourceInstanceFullName;

		private SearchyRuntimeSource _runtimeSource;

		public SearchyEntry_Source(SearchyRuntimeSource inRuntimeSource)
		{
			_runtimeSource = inRuntimeSource;
		}

		public void SetToSourceType(Type inType)
		{
			serializedSourceInstanceFullName = inType.FullName;
		}

		public override Texture2D Icon
		{
			get
			{
				if(SourceInstance != null)
				{
					return SourceInstance.Icon;
				}

				return null;
			}
		}

		public override bool AddDefaultTabCommand
		{
			get
			{
				return false;
			}
		}

		public override bool AddDefaultFavoriteCommand
		{
			get
			{
				return false;
			}
		}

		public SearchySourceBase SourceInstance
		{
			get
			{
				return _runtimeSource.SourceInstance;
			}
		}

		public override bool IsVisible
		{
			get
			{
				return _runtimeSource.SourceInstance.IsVisible;
			}
		}

		public override string DisplayName
		{
			get
			{
				if(_displayName == null)
				{
					if(_runtimeSource == null)
					{
						_displayName = "Missing source";
					}
					else
					{
						_displayName = _runtimeSource.SourceInstance.Name;
					}
				}

				return _displayName;
			}
		}

		public override string DisplayNamePost
		{
			get
			{
				return "...";
			}
		}

		public override void DrawEntry(Rect inRect, bool inIsSelectedInSearch, int inIndent)
		{
			base.DrawEntry(inRect, inIsSelectedInSearch, SourceInstance.GroupName == null ? 0 : (int)inRect.height-8+10);
		}

		public override void RefreshCommandList()
		{
			AddPrimaryCommand("...", Open_Command);
			AddRightCommand("...", Open_Command);
		}

		private void Open_Command()
		{
			SearchyCore.instance.AddSourceToChain(_runtimeSource.SourceInstance);
		}
	}
}
