using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public abstract class SearchyEntryBase
	{
		public SearchySourceBase parentSource;

		public virtual bool IsValid
		{
			get
			{
				return true;
			}
		}

		public virtual bool IsVisible
		{
			get
			{
				return true;
			}
		}

		protected string _hash = null;
		public virtual string Hash
		{
			get
			{
				if(_hash == null)
				{
					_hash = DisplayName + SubLabel;
				}

				return _hash;
			}
			
			set
			{
				_hash = value;
			}
		}

		protected string _displayNameLower = null;
		public virtual string DisplayNameLower
		{
			get
			{
				if(_displayNameLower == null && DisplayName != null)
				{
					_displayNameLower = DisplayName.ToLower();
				}

				return _displayNameLower;
			}
		}

		protected string _displayName;
		public virtual string DisplayName
		{
			get
			{
				return "No label set";
			}
		}

		public virtual string DisplayNamePost
		{
			get
			{
				return null;
			}
		}

		protected string _subLabelLower = null;
		public virtual string SubLabelLower
		{
			get
			{
				if(_subLabelLower == null && SubLabel != null)
				{
					_subLabelLower = SubLabel.ToLower();
				}

				return _subLabelLower;
			}
		}

		protected string _subLabel;
		public virtual string SubLabel
		{
			get
			{
				return null;
			}
		}
		
		protected bool _searchTextSet = false;
		protected string _searchText = "";
		public virtual string SearchText
		{
			get
			{
				if(!_searchTextSet)
				{
					_searchTextSet = true;
					if(AllowSubLabelSearch)
					{
						_searchText = (SubLabel != null ? SubLabel.ToLower() : "") + DisplayName.ToLower();
					}
					else
					{
						_searchText = DisplayName.ToLower();
					}
				}

				return _searchText;
			}
		}

		public virtual bool AllowSubLabelSearch
		{
			get
			{
				return true;
			}
		}

		public virtual Texture2D Icon
		{
			get
			{
				return null;
			}
		}

		public void DrawIcon(Rect inRect)
		{
			Rect iconRect = new Rect(inRect);

			if(Icon != null)
			{
				GUI.DrawTexture(iconRect, Icon, ScaleMode.ScaleToFit);
			}
		}

		private Rect _lastRect;
		public Rect LastRect
		{
			get
			{
				return _lastRect;
			}
			set
			{
				_lastRect = value;
			}
		}

		public virtual void DrawEntry(Rect inRect, bool inIsSelectedInSearch, int inIndent)
		{
			if(inIsSelectedInSearch)
			{
				GUI.DrawTexture(inRect, SearchyWindowStyles.EntrySelectedBG);
			}

			Rect iconRect = new Rect(inRect);
			iconRect.width = iconRect.height;
			iconRect.x += inIndent;
			iconRect.x += 6;
			iconRect.y += 6;
			iconRect.height -= 12;
			iconRect.width -= 12;

			DrawIcon(iconRect);

			Rect commandRect = new Rect(inRect);
			commandRect.x = inRect.x + inRect.width;
			
			if(inIsSelectedInSearch)
			{
				if(RightCommand != null && PrimaryCommand != null && RightCommand.TipLabel == PrimaryCommand.TipLabel)
				{
					commandRect.x -= DrawCommand(commandRect, '\u23ce'.ToString() + "/" + '\u2192'.ToString(), RightCommand.TipLabel);
				}
				else
				{
					if(RightCommand != null)
					{
						commandRect.x -= DrawCommand(commandRect, '\u2192'.ToString(), RightCommand.TipLabel);
					}

					if(PrimaryCommand != null)
					{
						commandRect.x -= DrawCommand(commandRect, '\u23ce'.ToString(), PrimaryCommand.TipLabel);
					}
				}

				if(SecondaryCommand != null)
				{
					#if UNITY_EDITOR_OSX
						commandRect.x -= DrawCommand(commandRect, '\u2318'.ToString() + "+" + '\u23ce'.ToString(), SecondaryCommand.TipLabel);
					#else
						commandRect.x -= DrawCommand(commandRect, "Ctrl" + "+" + '\u23ce'.ToString(), SecondaryCommand.TipLabel);
					#endif
				}

				if(TabCommand != null)
				{
					commandRect.x -= DrawCommand(commandRect, "Tab", TabCommand.TipLabel);
				}
			}

			Rect labelRect = new Rect(inRect);
			labelRect.x += Icon != null ? iconRect.x + iconRect.width + 4 : 4;
			labelRect.width = commandRect.x - labelRect.x - 10;

			GUIStyle entryStyle = inIsSelectedInSearch ? SearchyWindowStyles.EntrySelectedStyle : SearchyWindowStyles.EntryStandardStyle;
			if(SubLabel != null)
			{
				entryStyle.alignment = TextAnchor.UpperLeft;
			}
			else
			{
				entryStyle.alignment = TextAnchor.MiddleLeft;
			}

			string displayLabel = DisplayName;
			
			if(parentSource != null && parentSource.MatchFavorite(this))
			{
				displayLabel = '\u2605'.ToString() + " " + displayLabel;
			}

			if(DisplayNamePost != null)
			{
				displayLabel += DisplayNamePost;
			}
			
			GUIStyle labelStyle = inIsSelectedInSearch ? SearchyWindowStyles.EntrySelectedStyle : SearchyWindowStyles.EntryStandardStyle;
			GUIContent labelContent = new GUIContent(displayLabel);
			bool truncated = false;
			while(labelStyle.CalcSize(labelContent).x > labelRect.width - 10)
			{
				displayLabel = displayLabel.Substring(0, displayLabel.Length-1);
				labelContent.text = displayLabel;
				truncated = true;
			}
			if(truncated)
			{
				displayLabel += "...";
			}

			int searchIndex = displayLabel.ToLower().IndexOf(SearchyCore.instance.SearchText.ToLower());
			if(searchIndex != -1)
			{
				displayLabel = displayLabel.Substring(0, searchIndex) + "<Color=orange>" + displayLabel.Substring(searchIndex, SearchyCore.instance.SearchText.Length) + "</Color>" + displayLabel.Substring(searchIndex + SearchyCore.instance.SearchText.Length);
			}

			GUI.Label(labelRect, displayLabel, labelStyle);
			
			if(SubLabel != null)
			{
				Rect subLabelRect = new Rect();
				subLabelRect.x = labelRect.x;
				subLabelRect.y = labelRect.y + labelRect.height/2;
				subLabelRect.width = labelRect.width;
				subLabelRect.height = labelRect.height/2;
				
				Color oldColor = GUI.color;
				if(!inIsSelectedInSearch)
				{
					GUI.color = new Color(1,1,1,.5f);
				}

				string subLabel = SubLabel;
				GUIStyle subStyle = inIsSelectedInSearch ? SearchyWindowStyles.EntrySubSelectedStyle : SearchyWindowStyles.EntrySubStyle;
				GUIContent subContent = new GUIContent(subLabel);
				truncated = false;
				while(subStyle.CalcSize(subContent).x > subLabelRect.width - 10)
				{
					subLabel = subLabel.Substring(0, subLabel.Length-1);
					subContent.text = subLabel;
					truncated = true;
				}
				if(truncated)
				{
					subLabel += "...";
				}

				if(AllowSubLabelSearch && searchIndex == -1)
				{
					searchIndex = subLabel.ToLower().IndexOf(SearchyCore.instance.SearchText.ToLower());
					if(searchIndex != -1)
					{
						subLabel = subLabel.Substring(0, searchIndex) + "<Color=orange>" + subLabel.Substring(searchIndex, SearchyCore.instance.SearchText.Length) + "</Color>" + subLabel.Substring(searchIndex + SearchyCore.instance.SearchText.Length);
					}
				}

				GUI.Label(subLabelRect, subLabel, subStyle);

				GUI.color = oldColor;
			}
		}

		private float DrawCommand(Rect inRect, string inHotkeyLabel, string inTipLabel)
		{
			GUIContent nHotkeyContent = new GUIContent(inHotkeyLabel);
			GUIContent nTipContent = new GUIContent(inTipLabel);
			float width = SearchyWindowStyles.EntryMoreCenterLabelStyle.CalcSize(nHotkeyContent).x;
			width = Mathf.Max(width, SearchyWindowStyles.EntryMoreCenterLabelStyle.CalcSize(nTipContent).x);
			width += 4;
			Rect nRect = new Rect(inRect);
			nRect.width = width;
			nRect.x -= width + 2;
			if(inTipLabel == "")
			{
				GUI.Label(nRect, nHotkeyContent, SearchyWindowStyles.EntryMoreCenterLabelStyle);
			}
			else
			{
				nRect.height /= 2;
				nRect.y += 2;
				GUI.Label(nRect, nHotkeyContent, SearchyWindowStyles.EntryMoreUpperLabelStyle);
				nRect.y += nRect.height;
				nRect.y -= 4;
				GUI.Label(nRect, nTipContent, SearchyWindowStyles.EntryMoreLowerLabelStyle);
			}

			return width;
		}

		public virtual bool HasPreview
		{
			get
			{
				return false;
			}
		}

		public virtual void DrawPreview(Rect inRect)
		{
		}

		public virtual void FinishPreview()
		{
		}

		public virtual void CleanUp()
		{
		}

		[NonSerialized]
		private bool _refreshedCommands = false;

		[NonSerialized]
		private SearchyCommand _primaryCommand = null;
		public SearchyCommand PrimaryCommand
		{
			get
			{
				if(!_refreshedCommands)
				{
					InternalRefreshCommands();
				}

				return _primaryCommand;
			}
		}

		[NonSerialized]
		private SearchyCommand _secondaryCommand = null;
		public SearchyCommand SecondaryCommand
		{
			get
			{
				if(!_refreshedCommands)
				{
					InternalRefreshCommands();
				}

				return _secondaryCommand;
			}
		}

		[NonSerialized]
		private SearchyCommand _tabCommand = null;
		public SearchyCommand TabCommand
		{
			get
			{
				if(!_refreshedCommands)
				{
					InternalRefreshCommands();
				}

				return _tabCommand;
			}
		}

		[NonSerialized]
		private SearchyCommand _leftCommand = null;
		public SearchyCommand LeftCommand
		{
			get
			{
				if(!_refreshedCommands)
				{
					InternalRefreshCommands();
				}

				return _leftCommand;
			}
		}

		[NonSerialized]
		private SearchyCommand _rightCommand = null;
		public SearchyCommand RightCommand
		{
			get
			{
				if(!_refreshedCommands)
				{
					InternalRefreshCommands();
				}

				return _rightCommand;
			}
		}

		protected SearchyCommand MakeCommand(string inLabel, string inTipLabel, Action inActivateEvent)
		{
			SearchyCommandEvent nEvent = new SearchyCommandEvent();
			nEvent.AddListener(() => { inActivateEvent(); } );

			return new SearchyCommand() { DisplayLabel = inLabel, TipLabel = inTipLabel, activateEvent = nEvent, parentEntry = this };
		}

		protected void AddPrimaryCommand(string inTipLabel, Action inActivateEvent)
		{
			_primaryCommand = MakeCommand("", inTipLabel, inActivateEvent);
		}

		protected void AddSecondaryCommand(string inTipLabel, Action inActivateEvent)
		{
			_secondaryCommand = MakeCommand("", inTipLabel, inActivateEvent);
		}

		protected void AddTabCommand(string inTipLabel, Action inActivateEvent)
		{
			_tabCommand = MakeCommand("", inTipLabel, inActivateEvent);
		}

		protected void AddLeftCommand(string inTipLabel, Action inActivateEvent)
		{
			_leftCommand = MakeCommand("", inTipLabel, inActivateEvent);
		}

		protected void AddRightCommand(string inTipLabel, Action inActivateEvent)
		{
			_rightCommand = MakeCommand("", inTipLabel, inActivateEvent);
		}

		[NonSerialized]
		private List<SearchyCommand> _commandList = null;
		public List<SearchyCommand> CommandList
		{
			get
			{
				return _commandList;
			}
		}

		protected void AddListCommand(string inLabel, Action inActivateEvent)
		{
			AddListCommand(inLabel, null, inActivateEvent);
		}

		protected void AddListCommand(string inLabel, string inTipLabel, Action inActivateEvent)
		{
			_commandList.Add(MakeCommand(inLabel, inTipLabel, inActivateEvent));
		}

		public virtual bool AddDefaultTabCommand
		{
			get
			{
				return true;
			}
		}

		public virtual bool AddDefaultFavoriteCommand
		{
			get
			{
				return true;
			}
		}

		public virtual void RefreshCommandList()
		{
		}
		
		public virtual void InternalRefreshCommands()
		{
			if(_refreshedCommands)
			{
				return;
			}

			_refreshedCommands = true;

			_commandList = new List<SearchyCommand>();
			_leftCommand = null;
			_rightCommand = null;
			_primaryCommand = null;
			_secondaryCommand = null;
			_tabCommand = null;
			
			RefreshCommandList();
			RefreshCommands();
			
			if(parentSource != null)
			{
				if(AddDefaultTabCommand)
				{
					AddTabCommand("More", Command_ShowCommandList);
				}

				if(AddDefaultFavoriteCommand)
				{
					AddListCommand(parentSource.MatchFavorite(this) ? "Remove from Favorites" : "Add to Favorites", parentSource.MatchFavorite(this) ? "Remove selection from Favorites" : "Add selection to Favorites", Command_ToggleFavorite);
				}
			}
		}

		public void ClearCommands()
		{
			_refreshedCommands = false;
		}

		public virtual void RefreshCommands()
		{
			if(_commandList.Count > 0)
			{
				AddPrimaryCommand(_commandList[0].DisplayLabel, () => { _commandList[0].Activate(); } );
				AddRightCommand(_commandList[0].DisplayLabel, () => { _commandList[0].Activate(); } );
				
				if(_commandList.Count > 1)
				{
					AddSecondaryCommand(_commandList[1].DisplayLabel, () => { _commandList[1].Activate(); } );
				}
			}
		}

		public void Command_ShowCommandList()
		{
			SearchyRuntimeSource nRuntimeSource = SearchyCore.instance.AddSourceToChain(typeof(SearchySource_Commands), false);
			nRuntimeSource.SetOptionalData("CommandEntryParentSourceTypeFullName", parentSource.GetType().FullName);
			nRuntimeSource.SetOptionalData("CommandEntryTypeFullName", this.GetType().FullName);
			nRuntimeSource.SetOptionalData("CommandEntryHash", Hash);
		}

		public void Command_ToggleFavorite()
		{
			parentSource.ToggleFavorite(this);
		}

		public void ActivatePrimaryCommand()
		{
			if(PrimaryCommand == null)
			{
				return;
			}

			PrimaryCommand.Activate();
		}

		public void ActivateSecondaryCommand()
		{
			if(SecondaryCommand == null)
			{
				return;
			}

			if(parentSource != null)
			{
				parentSource.AddRecent(this);
			}

			SecondaryCommand.Activate();
		}

		public void ActivateTabCommand()
		{
			if(TabCommand == null)
			{
				return;
			}

			TabCommand.Activate();
		}

		public void ActivateRightCommand()
		{
			if(RightCommand == null)
			{
				return;
			}

			RightCommand.Activate();
		}

		public void ActivateLeftCommand()
		{
			if(LeftCommand == null)
			{
				return;
			}

			LeftCommand.Activate();
		}
	}
}
