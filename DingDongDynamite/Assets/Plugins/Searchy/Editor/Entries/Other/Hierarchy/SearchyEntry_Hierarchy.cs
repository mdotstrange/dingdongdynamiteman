using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	public class SearchyEntry_Hierarchy : SearchyEntryBase
	{
		public int instanceID = -1;

		public override bool IsValid
		{
			get
			{
				return ObjectInstance != null;
			}
		}

		public override string Hash
		{
			get
			{
				if(_hash == null)
				{
					_hash = instanceID.ToString();
				}

				return _hash;
			}
			set
			{
				instanceID = Int32.Parse(value);
			}
		}

		// TODO
		// Favorites not supported yet, instanceIDs are not persistent in Unity
		public override bool AddDefaultFavoriteCommand
		{
			get
			{
				return false;
			}
		}

		private bool _objectInstanceSet = false;
		private GameObject _objectInstance = null;
		private GameObject ObjectInstance
		{
			get
			{
				if(!_objectInstanceSet)
				{
					_objectInstanceSet = true;
					_objectInstance = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
				}

				return _objectInstance;
			}
		}

		public override string DisplayName
		{
			get
			{
				if(!IsValid)
				{
					return "Not in current scene";
				}

				if(ObjectInstance.transform.childCount > 0)
				{
					return ObjectInstance.name;
				}
				else
				{
					return ObjectInstance.name;
				}
			}
		}

		public override string DisplayNamePost
		{
			get
			{
				if(!IsValid)
				{
					return null;
				}

				if(ObjectInstance.transform.childCount > 0)
				{
					return "...";
				}

				return null;
			}
		}


		public override string SubLabel
		{
			get
			{
				if(!IsValid)
				{
					return "";
				}

				if(_subLabel == null)
				{
					List<string> parentNames = new List<string>();
					Transform cParent = ObjectInstance.transform.parent;
					while(cParent != null)
					{
						parentNames.Add(cParent.name);
						cParent = cParent.parent;
					}

					for(int i = parentNames.Count-1; i >= 0; i--)
					{
						_subLabel += parentNames[i];
						if(i > 0)
						{
							_subLabel += "/";
						}
					}

					Component[] components = ObjectInstance.GetComponents<Component>();

					_subLabel += " [";
					if(components.Length > 1)
					{
						for(int i = 0; i < components.Length; i++)
						{
							Component cComponent = components[i];

							if(cComponent is Transform || cComponent == null)
							{
								continue;
							}
							
							_subLabel += cComponent.GetType().Name;
							if(i < components.Length - 1)
							{
								_subLabel += ", ";
							}
						}
					}
					else
					{
						_subLabel += "Transform";
					}
					_subLabel += "]";
				}
				return _subLabel;
			}
		}

		public override string SearchText
		{
			get
			{
				if(!_searchTextSet)
				{
					_searchTextSet = true;

					if(!IsValid)
					{
						_searchText = "Missing";
						return _searchText;
					}

					_searchText = DisplayName.ToLower() + " ";
					Component[] components = ObjectInstance.GetComponents<Component>();
					if(components.Length > 1)
					{
						foreach(Component cComponent in components)
						{
							if(cComponent is Transform || cComponent == null)
							{
								continue;
							}
							
							_searchText += cComponent.GetType().Name.ToLower() + " ";
						}
					}
					else
					{
						_searchText += "transform";
					}
				}

				return _searchText;
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowStyles.IconHierarchy;
			}
		}

		public override void RefreshCommandList()
		{
			if(!IsValid)
			{
				return;
			}

			if(ObjectInstance.transform.childCount > 0)
			{
				AddListCommand("...", Command_Filter);
			}

			AddListCommand("Select", "Select in project view", Command_SelectHierarchy);
		}

		private void Command_SelectHierarchy()
		{
			if(parentSource != null)
			{
				parentSource.AddRecent(this);
			}

			SearchyWindow.CloseWindow();

			System.Type typeProjectWindow = typeof(EditorWindow).Assembly.GetType("UnityEditor.SceneHierarchyWindow");
			EditorWindow projectWindow = EditorWindow.GetWindow(typeProjectWindow);
			MethodInfo methodFrameObject = typeProjectWindow.GetMethod("FrameObject");
			methodFrameObject.Invoke(projectWindow, new object[] { instanceID, false } );
			Selection.instanceIDs = new int[] { instanceID };
		}

		protected void Command_Filter()
		{
			SearchyRuntimeSource nRuntimeSource = SearchyCore.instance.AddSourceToChain(typeof(SearchySource_Hierarchy), false);
			nRuntimeSource.SetOptionalData("FilterInstanceID", instanceID);
		}
	}
}
