using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public static class SearchyWindowStyles
	{
		private static string _iconSearchString = "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAFHklEQVRoBd2a26tVVRTGOyV5Q+hFLaSSTE8XUUxMSkkRitDUqDB7CEExjAgCe0iwt8AXJQTR8w+EmOaFMjQVUbv64C0CUQmkqKwDURIer6fft90LFuvMMfe6jL3d+wz4ztprjDHHGN9Zc80151yrq7+//66SMox2c8FsMBU8Ah4Aafmdk5/BKfA1OAL6wB2TrhKEp1DtKvAGuK9g5X/jvxX0gB8LtvVxF+GcmIHfYeAlhwg0HeTN7+KXJ8gIitoMbgJvUcxNYDjIU0tln0Zdupt+tANM9ulPZpQzWF4D500PJ0OMsAajz0HR+7Rsabq/F4JvygbI084iPJ3Gh8GoBkFuYv8efAl+AH+Av4BkDLgfzATz68d7OMbkX4zPg+Mxp0q2wL0zAd0lEJM+jBvAaJD3vhqD78dAbWPSi7Eb5I1byC/rPIxEJ2PVYNsBHgLZtnnPx9N2F4iJahgK8sbM7Zd13Bip4jq2VY5FvEMsxbRkHYZsfZXP0wGmkuCGkf0a+kVNKGAxMS3SyjnZO2ea8D6CW7IcQ9rX8/dKKyn6Pd55k8KnRZJu804aiKdxISS3UKrnJXVWPt5dH+LfNoZ6TfTfN2ye6tUEuxoI2IXurYC+tEqE7wWa5YRkM8pfQgZn3UXiaUERkqUoGz2/Q+2COk085mE5FLDeQDcO/BmwNUOlScqvIEROsz6XGZiu8BwQkm9Rtoqs8muWptlaSJ4LKcvoRHia0VDz6FaLlfMpr0JEeKIRzPpvG+4uaivnoy7RCSLCY41g6mKtFm0JhURjiYuI8EgjUjsRHm7UWFgtwnrWhcTSh3y9dKonJJY+5BvVKdBlwyO7A2m4uaqtnP94ZRFh69Gj52KrxSJ8yasQET5nBNPDvtUyy0h41tAXVouwNslD8mJI2WSdlfOkV14RPmIEewa99qVaJbqFnjaSHTP0hdUirGC9gZaa064N6Jul+pDAoXn0b+itCUnhWkT4OthltFyJ/kHD5ql+mGArjICq7ZZhK66uL65jGwDbPRfgRqyd6EOiNxOPgcoL/yRGOlBsi+fdpEETju8R0xLthKRrrPw7HSC2iafNPW24pf09fr9MzNjG4STvnNmi22mb9iNvsoqXJdwuG/GXKW5UKwjrH3CnX7VQQk328tf97UO7vUzLPmb2ongVXM0ayp5bhBXvSbAfuC2+FbSEHKXNfPBfibYDmmjiYclPGOaCE5aDo74vEksbeNvA0IhPblOMsIJcAM+CdUAzMm/R++X1QL3oYCT4AmyfgeqkGRiyI7V1/gS+u4Fef1QVzaA0g3scJPn0nccBEJMvMFYayJJkRY6aoOhDlN5YZYZNbXqANV0U6a+Mtom6EunYoBXpYTWTVjZaQs4D+qxhAkgvJzXIaCPwPDgNvquj0a2hDbvd4AVgSenRuwphqxgPvd53fQoWR4IdqNuvRHwGmBoNWgMatEhxjTxLgK60Jfr4ZQ9Qj8gt7UpYBET6deBKup0JJ6Rdr3S7ExZpDXJupDuBcJq0tRUln1z3dKcQTkjrnq5EupMIp0nv1Ikh0SvdaYTFMbmntxqEpTZJdyJhEdKi401QmHS7zrREKo9oRqZV1EsRZ01DXwF6rte+ANCxU0UktCNifRsiXlpartEPSadf4dssbn9rtp2TRYkiczzHebd0nXoPZ/jUuqu67SdZQ/281p31e7AQFhcNZMuBFhRZ2ZIoBkuXTvjoOAR8AJYBXdlNoAewCz947mFxySWDqUvnIvw/lZYRTU0c/98AAAAASUVORK5CYII=";
		private static Texture2D _iconSearch;
		public static Texture2D IconSearch
		{
			get
			{
				if(_iconSearch == null)
				{
					byte[] b64_bytes = System.Convert.FromBase64String(_iconSearchString);
					_iconSearch = new Texture2D(1,1);
					_iconSearch.LoadImage(b64_bytes);
					_iconSearch.filterMode = FilterMode.Point;
					_iconSearch.hideFlags = HideFlags.DontSave;
				}
				return _iconSearch;
			}
		}

		private static string _iconFolderString = "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAABDklEQVRoBe2awQ3CMBAEMaIEyqAXXlRGlUj0YHIS4WGJH8xYyVqyQvIZdueSfNJ674c9reOewlbWBN668Rjem+F6ZP96X2cqsQ2vpX+8o55L4MuyHzMEJ+7h8xL0PkPY+g9E4OLc3rt+q4sY6TXgFKNNGa7QU4w2GbhC66NNjnQFrqWONm24AqujbRiu0PRqK9AwvLKVYwIrtYPQGAbLVlAxrNQOQmMYLFtBxbBSOwiNYbBsBRXDSu0gNIbBshVUDCu1g9AYBstWUDGs1A5CYxgsW0HFsFI7CI1hsGwFFcNK7SA0hsGyFVQMK7WD0BgGy1ZQMazUDkJjGCxbQZ0G6ueLteH6Zk4z0ptR+SXIC86AMevFTnOCAAAAAElFTkSuQmCC";
		private static Texture2D _iconFolder;
		public static Texture2D IconFolder
		{
			get
			{
				if(_iconFolder == null)
				{
					byte[] b64_bytes = System.Convert.FromBase64String(_iconFolderString);
					_iconFolder = new Texture2D(1,1);
					_iconFolder.LoadImage(b64_bytes);
					_iconFolder.filterMode = FilterMode.Bilinear;
					_iconFolder.wrapMode = TextureWrapMode.Clamp;
					_iconFolder.hideFlags = HideFlags.DontSave;
				}
				return _iconFolder;
			}
		}

		private static string _iconHierarchyString = "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAABHUlEQVRoBe2YQQ7CUAhErfH+V65bFsM3kSkQGFeVGmB4U7563ff9cl7uDefzNnzZN52u352ayehFgjOmXFlDhCunn1FbhDOmXFlDhCunn1FbhDOmXFlDhCunn1F7HeHPYaptf+Idev55ax3hdYJPlvbsYf8Jedr2tpbXjxeHva0jLMGeP6bERXgKSU/HP1va5opsUZsn7VqWTht1UaGopeHhTtRCf2RkaSKdlqlEuCUWYlPrCEe3NHH2MBX9FFhHWIKhsQYFRXgQTCil+5aOfJeGG16Whj4YFBThQTChlHWEo1s6skUhgaeD6wivExy1NDzcibakPzLrCEsw0Y4tU4lwSyzEpqJbmtgKTEU/BWRpOOdBQREeBBNKWUf4C47dC4jzU7uTAAAAAElFTkSuQmCC";
		private static Texture2D _iconHierarchy;
		public static Texture2D IconHierarchy
		{
			get
			{
				if(_iconHierarchy == null)
				{
					byte[] b64_bytes = System.Convert.FromBase64String(_iconHierarchyString);
					_iconHierarchy = new Texture2D(1,1);
					_iconHierarchy.LoadImage(b64_bytes);
					_iconHierarchy.filterMode = FilterMode.Bilinear;
					_iconHierarchy.hideFlags = HideFlags.DontSave;
				}
				return _iconHierarchy;
			}
		}

		private static string _iconRecentsString = "iVBORw0KGgoAAAANSUhEUgAAADkAAAA8CAYAAADc1RI2AAAFfklEQVRoBe2aWahVVRjHu1ZON7pNUkgIDlGhQU+maVhUYjQJ+WBPlyhCbIA0qWy6RiJI0FNvvTRARD1FGtlEVoKKD1ZiAw03yLSbqEk5lN5+/8O+y3XWWd8+e5+997n33Pzgf/a3/t9a33DOHtZe63QNDg6eMdplzGgvUPWdLnK0/Mqnf8nR8kueVVEh4/E7D9wArkxwCcch0S39IDgEdoOvwQ7wGTgCSpWuEh8hZ5PZLaAXLAITQV5RgSr0FfA2OA6Ki4osiHMYvxL8CsqUvTh7BnSDQjkWGTyG4PeC30GVsgfnilM76zhmyflxv1+WAbE+03GyBbRTPiTYFBDLx+dWJ0k5zikZBg/1XULfg4mjdh8U904wlEt4fMpLyNmckjLQ77OCfic9R2mqEnoL3A+uA5PB+R4moc8Hy8EbYD/IIifotAr4eUnX9euLszsFazN9je8hRd+A7XYwDjTz6dvPpP+tYBPI8kWu9fz3oYfifDuFHmn6I6GHSPsLuDlN/KTF8G1z8bM1EiOkHoV4LiSTtvPnFAyWfhe2tG/2L+x5735WLJ/X3VSPpmOgFXG+nIKXmH4Z/KGUCD9hmwliY8viZuP/N5BXXHyn4CHU9RzcluJ5FzbdTMJxVbSnEue7lFxiJpeHUyLJPhQbmXAKOCkyJs1fUZu+0J+T+FkOLp71qtXDhLHPmDRqfrkEDBj2qug9OH6vFedWkQ/g7ALD4cPwXxq2Kuk+nC9rKUDklJsAN2CcD3pM5JlDulMmEieP7VkjnzTa+XeKl8TSlJFlPQdjcS0unMmkpFdncv6c4hW5sa7rqcb7Xp/YuCq4p0+Fz625fJySFNDN8bjh7rakTzimqvZ6I4+stMsrvPHM5cLWG34o+yE2hWSF7dX4XlWW/7DI+Ybjj+DLWYowAnj0E+hrvXZhNSzyCsPjpwZfBb0Op10lwOUWFnm5s9Qru+qbndUKi7zYSP8Hg+8IOizyXCPrAwbfEXRYpBaFY3I0RnYKFxapyXdMJsTITuHCIg8biZ9n8B1Bh0XuM7KebvAdQYdFfmtkPdPgO4IOi/zGyHqBwQ83/S4JaIcsxGN+YmGRn/tGT78RfazXHgmq7hM3G4nU1REWuYVBxyIDL4TTttxIksUkE/vitee53U80LPJvjB/7HTx9uacPt6q5rbUUsgFb3ctEWKSSf1UfEVkINyfCDwd1B0GvMQK/GfK19ZqA1IP/F3BRwKu5Deid86QawyT6YXaCWZH4ynsaOOHbNCAUzXpeDMmkPZvjg4atXfSTBIoVqPgvgboCRbolgmBpo4e2tZV2BNvVQX/LT9n8TcT9F8REO9ITQUPMBsLrtCzmKeH6OV7q9U3zU5ZtBvH2JfFjB206RWNFyaTzSNoLmUVO+qUs+QSDuR6cVqRsWXa1rqJfMz9F7Nqr/ANYcgDDNGDGMA3eoGb7k7pGtUmrneIs/rL2GYu/58E/wBLtmy4GqT5Tjd7gLDvN2uab543J6jvsp9NuIdgJmok2acPxDe0GImXQmmYRE/tmjncD7ank8a+F7V6QpTi61bbRM/mvXay1Z0m2jxV0ewFoWtVM/qSDFqQ/ALuBFsO0VqTncDfQf+2mAM2iNNG+FowDzURvHCuB9SxvGJ+3SDnQ3uTLoEeNNov+dNgL3skTt5Ui5V8rBa8BTfHaJZsJdA/4MW/A2LQuiw+detpSuA8MZBlQoM9exuqN43qQu0DGmNO6TBc0F7/6VfUvyX5863860alaEjtTnq2errUvKPjQbpherHXNLAKt/N/1KOM2gteBrrvGyTZkXimzSD/2eBr65/ICMANMBZOBVhh0Z9VLre6+/eB78BXYCvQqdxiUKlUVWWqSRZ21euMpGret4/8XRf4HiS+8RsV18QkAAAAASUVORK5CYII=";
		private static Texture2D _iconRecents;
		public static Texture2D IconRecents
		{
			get
			{
				if(_iconRecents == null)
				{
					byte[] b64_bytes = System.Convert.FromBase64String(_iconRecentsString);
					_iconRecents = new Texture2D(1,1);
					_iconRecents.LoadImage(b64_bytes);
					_iconRecents.filterMode = FilterMode.Bilinear;
					_iconRecents.hideFlags = HideFlags.DontSave;
				}
				return _iconRecents;
			}
		}

		private static GUIStyle _searchEmptyStyle;
		public static GUIStyle SearchEmptyStyle
		{
			get
			{
				if(_searchEmptyStyle == null)
				{
					_searchEmptyStyle = new GUIStyle(EditorStyles.largeLabel);
					_searchEmptyStyle.fontSize = 20;
					_searchEmptyStyle.normal.textColor = Color.grey;
				}

				return _searchEmptyStyle;
			}
		}

		private static GUIStyle _searchStyle;
		public static GUIStyle SearchStyle
		{
			get
			{
				if(_searchStyle == null)
				{
					_searchStyle = new GUIStyle(SearchEmptyStyle);
					_searchStyle.normal.textColor = Color.white;
				}

				return _searchStyle;
			}
		}

		private static GUIStyle _entryStandardStyle;
		public static GUIStyle EntryStandardStyle
		{
			get
			{
				if(_entryStandardStyle == null)
				{
					_entryStandardStyle = new GUIStyle(EditorStyles.largeLabel);
					_entryStandardStyle.fontSize = 14;
					_entryStandardStyle.alignment = TextAnchor.MiddleLeft;
					_entryStandardStyle.contentOffset = new Vector2(0, 0);
					_entryStandardStyle.richText = true;
				}

				return _entryStandardStyle;
			}
		}

		private static GUIStyle _entrySelectedStyle;
		public static GUIStyle EntrySelectedStyle
		{
			get
			{
				if(_entrySelectedStyle == null)
				{
					_entrySelectedStyle = new GUIStyle(EntryStandardStyle);
					_entrySelectedStyle.normal.textColor = Color.white;
					_entrySelectedStyle.border = new RectOffset(3,2,2,2);
				}

				return _entrySelectedStyle;
			}
		}

		private static Texture2D _entrySelectedBG;
		public static Texture2D EntrySelectedBG
		{
			get
			{
				if(_entrySelectedBG == null)
				{
					_entrySelectedBG = new Texture2D(2, 2);
					_entrySelectedBG.hideFlags = HideFlags.DontSave;
					Color[] colors = new Color[4];
					Color selectedColor = new Color(.25f, .40f, .87f, .57f);
					for(int i = 0; i < 4; i++)
					{
						colors[i] = selectedColor;
					}
					_entrySelectedBG.SetPixels(colors);
					_entrySelectedBG.Apply();
				}
				
				return _entrySelectedBG;
			}
		}

		private static GUIStyle _entrySubStyle;
		public static GUIStyle EntrySubStyle
		{
			get
			{
				if(_entrySubStyle == null)
				{
					_entrySubStyle = new GUIStyle(EditorStyles.largeLabel);
					_entrySubStyle.alignment = TextAnchor.MiddleLeft;
					_entrySubStyle.fontSize = 11;
					_entrySubStyle.contentOffset = new Vector2(0, 0);
					_entrySubStyle.richText = true;
				}

				return _entrySubStyle;
			}
		}
		
		private static GUIStyle _entrySubSelectedStyle;
		public static GUIStyle EntrySubSelectedStyle
		{
			get
			{
				if(_entrySubSelectedStyle == null)
				{
					_entrySubSelectedStyle = new GUIStyle(EntrySubStyle);
					_entrySubSelectedStyle.normal.textColor = Color.white;
				}

				return _entrySubSelectedStyle;
			}
		}

		private static GUIStyle _tipStyle;
		public static GUIStyle TipStyle
		{
			get
			{
				if(_tipStyle == null)
				{
					_tipStyle = new GUIStyle(EditorStyles.largeLabel);
					_tipStyle.fontSize = 10;
					_tipStyle.alignment = TextAnchor.MiddleLeft;
					_tipStyle.contentOffset = new Vector2(4, 0);
					_tipStyle.richText = true;
				}

				return _tipStyle;
			}
		}

		private static GUIStyle _entryMoreStyle;
		public static GUIStyle EntryMoreStyle
		{
			get
			{
				if(_entryMoreStyle == null)
				{
					_entryMoreStyle = new GUIStyle(EditorStyles.largeLabel);
					_entryMoreStyle.fontSize = 12;
					_entryMoreStyle.alignment = TextAnchor.MiddleCenter;
				}

				return _entryMoreStyle;
			}
		}

		private static GUIStyle _entryMoreCenterLabelStyle;
		public static GUIStyle EntryMoreCenterLabelStyle
		{
			get
			{
				if(_entryMoreCenterLabelStyle == null)
				{
					_entryMoreCenterLabelStyle = new GUIStyle(EditorStyles.largeLabel);
					_entryMoreCenterLabelStyle.fontSize = 12;
					_entryMoreCenterLabelStyle.alignment = TextAnchor.MiddleCenter;
				}

				return _entryMoreCenterLabelStyle;
			}
		}

		private static GUIStyle _entryMoreUpperLabelStyle;
		public static GUIStyle EntryMoreUpperLabelStyle
		{
			get
			{
				if(_entryMoreUpperLabelStyle == null)
				{
					_entryMoreUpperLabelStyle = new GUIStyle(EditorStyles.largeLabel);
					_entryMoreUpperLabelStyle.fontSize = 12;
					_entryMoreUpperLabelStyle.alignment = TextAnchor.LowerCenter;
				}

				return _entryMoreUpperLabelStyle;
			}
		}

		private static GUIStyle _entryMoreLowerLabelStyle;
		public static GUIStyle EntryMoreLowerLabelStyle
		{
			get
			{
				if(_entryMoreLowerLabelStyle == null)
				{
					_entryMoreLowerLabelStyle = new GUIStyle(EditorStyles.largeLabel);
					_entryMoreLowerLabelStyle.fontSize = 10;
					_entryMoreLowerLabelStyle.alignment = TextAnchor.MiddleCenter;
				}

				return _entryMoreLowerLabelStyle;
			}
		}

		private static GUIStyle _entryHeaderStyle;
		public static GUIStyle EntryHeaderStyle
		{
			get
			{
				if(_entryHeaderStyle == null)
				{
					_entryHeaderStyle = new GUIStyle(EditorStyles.boldLabel);
					_entryHeaderStyle.contentOffset = new Vector2(15f/2, 0);
					_entryHeaderStyle.fontSize = 14;
					_entryHeaderStyle.alignment = TextAnchor.MiddleLeft;
					_entryHeaderStyle.normal.textColor = Color.white;
				}

				return _entryHeaderStyle;
			}
		}
	}
}