using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	public static class SearchyWindowUtils
	{
		private static MethodInfo _methodLoadIcon;
		private static Dictionary<string, Texture2D> _methodLoadIconCache = new Dictionary<string, Texture2D>();
		public static Texture2D LoadEditorIcon(string inIconName)
		{
			if(_methodLoadIcon == null)
			{
				_methodLoadIcon = typeof(EditorGUIUtility).GetMethod("LoadIcon", BindingFlags.NonPublic | BindingFlags.Static);
			}

			if(!_methodLoadIconCache.ContainsKey(inIconName))
			{
				_methodLoadIconCache.Add(inIconName, _methodLoadIcon.Invoke(null, new object[] { inIconName }) as Texture2D);
			}

			return _methodLoadIconCache[inIconName];
		}

		public static Texture2D LoadEditorIconWithExtension(string inExtension)
		{
			return UnityEditorInternal.InternalEditorUtility.GetIconForFile("File." + inExtension);
		}

		public static void DrawHorizontalSeparator(float inY, float inWidth)
		{
			Color oldColor = GUI.color;
			Color nColor = Color.black;
			GUI.color = nColor;
			Rect cRect = new Rect(0, inY, inWidth, 1);
			GUI.DrawTexture(cRect, EditorGUIUtility.whiteTexture);
			GUI.color = oldColor;
		}

		public static void DrawVerticalSeparator(float inX, float inY, float inHeight)
		{
			Color oldColor = GUI.color;
			Color nColor = Color.black;
			GUI.color = nColor;
			Rect cRect = new Rect(inX, inY, 1, inHeight);
			GUI.DrawTexture(cRect, EditorGUIUtility.whiteTexture);
			GUI.color = oldColor;
		}
	}
}
