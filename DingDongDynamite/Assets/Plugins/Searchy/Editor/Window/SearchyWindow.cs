using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	public class SearchyWindow : EditorWindow
	{
		public static SearchyWindow instance = null;
		private static EditorWindow _lastFocusedWindow = null;
		private static double _lastOpenedTime;
		
		private static float _padding = 15;
		private static float _searchFieldHeight = 30;
		private static float _selectedEntryIconSize = 30;
		private static float _itemHeight = 36;
		
		private static int _itemMinCount = 4;
		private static int _itemMaxCount = 12;

		private static int _clearAfterUnusedSeconds = 15;
		
		private static Rect? _lastWindowSize = null;
		private static float _windowWidth = -1;
		private static float WindowWidth
		{
			get
			{
				if(_windowWidth == -1)
				{
					_windowWidth = EditorPrefs.GetFloat("Searchy_Settings_WindowWidth", 800);
				}
				return _windowWidth;
			}

			set
			{
				_windowWidth = value;
				EditorPrefs.SetFloat("Searchy_Settings_WindowWidth", _windowWidth);
			}
		}

		private static float _windowHeight = -1;
		private static float WindowHeight
		{
			get
			{
				if(_windowHeight == -1)
				{
					_windowHeight = EditorPrefs.GetFloat("Searchy_Settings_WindowHeight", _padding + _searchFieldHeight + _padding + _itemHeight * _itemMaxCount);
				}
				return _windowHeight;
			}

			set
			{
				_windowHeight = value;
				EditorPrefs.SetFloat("Searchy_Settings_WindowHeight", _windowHeight);
			}
		}

		private static bool _previewPanelResizing = false;
		private static float _previewPanelWidth = -1;
		private static float PreviewPanelWidth
		{
			get
			{
				if(_previewPanelWidth == -1)
				{
					_previewPanelWidth = EditorPrefs.GetFloat("Searchy_Settings_PreviewPanelWidth", 200);
				}
				return _previewPanelWidth;
			}

			set
			{
				_previewPanelWidth = value;
				EditorPrefs.SetFloat("Searchy_Settings_PreviewPanelWidth", _previewPanelWidth);
			}
		}

		private static Vector2 _previewScrollPos;

		private static bool _scrollToSelectedOnNextFrame = false;

		private SearchyWindow()
		{
			EditorApplication.update += Init;
		}

		public static void CloseWindow()
		{
			if(instance != null)
			{
				instance.CleanUp();

				if(!IsDocked)
				{
					_lastOpenedTime = Time.realtimeSinceStartup;
					instance.Close();
				}
				else if(_lastFocusedWindow != null)
				{
					_lastFocusedWindow.Focus();
					_lastFocusedWindow = null;
				}
			}
		}

		[MenuItem("Window/Searchy/Searchy %#o")]
		public static void OpenWindow()
		{
			if(EditorWindow.focusedWindow != instance)
			{
				_lastFocusedWindow = EditorWindow.focusedWindow;
			}

			instance = EditorWindow.GetWindow<SearchyWindow>(false, "Searchy");

			if(SearchyCore.instance != null)
			{
				if(!IsDocked)
				{
					if(Time.realtimeSinceStartup - _lastOpenedTime >= _clearAfterUnusedSeconds)
					{
						SearchyCore.instance.ClearSourceChain();
					}
					
					_lastOpenedTime = Time.realtimeSinceStartup;
				}
			}
		}

		private static void Init()
		{
			UnityEngine.Object[] windows = Resources.FindObjectsOfTypeAll(typeof(EditorWindow));
			foreach(var cWindow in windows)
			{
				if(cWindow.GetType() == typeof(SearchyWindow))
				{
					instance = cWindow as SearchyWindow;
				}
			}

			if(instance == null)
			{
				return;
			}

			EditorApplication.update -= Init;

			if(!IsDocked)
			{
				instance.UpdateWindowSize();
				instance.CenterWindow();
			}
		}

		private static bool IsDocked
		{
			get
			{
				MethodInfo isDockedMethod = typeof(EditorWindow).GetProperty("docked", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetGetMethod(true);
				return (bool)isDockedMethod.Invoke(instance, null);
			}
		}

		private void CenterWindow()
		{
			Rect? mainWindowPos = null;

			Type typeContainerWindow = null;
			Assembly[] allAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			foreach(Assembly cAssembly in allAssemblies)
			{
				Type[] cTypes = cAssembly.GetTypes();
				foreach(Type cType in cTypes)
				{
					if(cType.Name == "ContainerWindow" && cType.IsSubclassOf(typeof(ScriptableObject)))
					{
						typeContainerWindow = cType;
						break;
					}
				}

				if(typeContainerWindow != null)
				{
					break;
				}
			}
			
			if(typeContainerWindow == null)
			{
				Debug.LogError("Searchy: Unable to find ContainerWindow type");
				return;
			}

			FieldInfo fieldShowMode = typeContainerWindow.GetField("m_ShowMode", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
			PropertyInfo propPosition = typeContainerWindow.GetProperty("position", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			
			if(fieldShowMode == null || propPosition == null)
			{
				Debug.LogError("Searchy: Unable to find ContainerWindow properties");
				return;
			}
			
			UnityEngine.Object[] containerWindows = Resources.FindObjectsOfTypeAll(typeContainerWindow);
			foreach(UnityEngine.Object cWindowObj in containerWindows)
			{
				int cShowMode = (int)fieldShowMode.GetValue(cWindowObj);
				if(cShowMode == 4)
				{
					mainWindowPos = (Rect)propPosition.GetValue(cWindowObj, null);
				}
			}
			
			if(!mainWindowPos.HasValue)
			{
				Debug.LogError("Searchy: Unable to find MainWindow");
				return;
			}

			Rect nPos = position;

			float w = (mainWindowPos.Value.width - nPos.width)*0.5f;
			float h = (mainWindowPos.Value.height - nPos.height)*0.5f;
			nPos.x = mainWindowPos.Value.x + w;
			nPos.y = mainWindowPos.Value.y + h;
			
			position = nPos;
		}

		private void ProcessInput()
		{
			Event current = Event.current;

			if(current.commandName != "")
			{
				switch(current.commandName)
				{
					case "SelectAll":
					{
						current.Use();
						
						TextEditor txt = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
						txt.OnFocus();
						txt.SelectAll();

						return;
					}
				}
			}

			if(!current.isKey || current.type != EventType.KeyDown)
			{
				return;
			}

			switch(current.keyCode)
			{
				case KeyCode.Escape:
				{
					current.Use();
					CloseWindow();
					break;
				}

				case KeyCode.Return:
				case KeyCode.KeypadEnter:
				{
					current.Use();
					SearchyEntryBase selectedEntry = SelectedEntry;
					if(selectedEntry == null)
					{
						return;
					}

					if(current.command || current.control)
					{
						if(selectedEntry.SecondaryCommand != null)
						{
							selectedEntry.ActivateSecondaryCommand();
						}
						else
						{
							selectedEntry.ActivatePrimaryCommand();
						}
					}
					else
					{
						selectedEntry.ActivatePrimaryCommand();
					}
					break;
				}

				case KeyCode.Backspace:
				{
					if(string.IsNullOrEmpty(SearchyCore.instance.SearchText))
					{
						if(SearchyCore.instance.SourceChain.Count > 1)
						{
							SearchyCore.instance.RemoveSourceFromChain();
							current.Use();
						}
					}

					break;
				}

				case KeyCode.LeftArrow:
				{
					bool leftOfTextEditor = false;

					TextEditor txt = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
					if(txt != null && txt.cursorIndex == 0)
					{
						leftOfTextEditor = true;
					}

					if(string.IsNullOrEmpty(SearchyCore.instance.SearchText))
					{
						if(SelectedEntry != null && SelectedEntry.LeftCommand != null)
						{
							SelectedEntry.ActivateLeftCommand();
							current.Use();
						}
						else if(SearchyCore.instance.SourceChain.Count > 1)
						{
							SearchyCore.instance.RemoveSourceFromChain();
							current.Use();
						}
					}
					else if(leftOfTextEditor)
					{
						if(SearchyCore.instance.SourceChain.Count > 1)
						{
							SearchyCore.instance.RemoveSourceFromChain();
							current.Use();
						}
					}
					break;
				}

				case KeyCode.UpArrow:
				{
					current.Use();
					if(current.command)
					{
						SelectHome();
					}
					else
					{
						SelectPreviousEntry();
					}
					break;
				}

				case KeyCode.DownArrow:
				{
					current.Use();
					if(current.command)
					{
						SelectEnd();
					}
					else
					{
						SelectNextEntry();
					}
					break;
				}

				case KeyCode.PageUp:
				{
					current.Use();
					SelectPageUp();
					break;
				}

				case KeyCode.PageDown:
				{
					current.Use();
					SelectPageDown();
					break;
				}

				case KeyCode.Home:
				{
					current.Use();
					SelectHome();
					break;
				}

				case KeyCode.End:
				{
					current.Use();
					SelectEnd();
					break;
				}

				case KeyCode.RightArrow:
				{
					if(SelectedEntry == null)
					{
						break;
					}
					
					TextEditor txt = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
					if(txt == null)
					{
						break;
					}
					if(txt.cursorIndex != txt.text.Length)
					{
						break;
					}
					
					current.Use();

					SelectedEntry.ActivateRightCommand();
					break;
				}

				case KeyCode.Tab:
				{
					if(SelectedEntry == null)
					{
						break;
					}

					current.Use();

					SelectedEntry.ActivateTabCommand();
					break;
				}
			}
		}

		public void CleanUp()
		{
			if(_lastDrawnSelectedEntry != null)
			{
				if(_lastDrawnSelectedEntry.HasPreview)
				{
					_lastDrawnSelectedEntry.FinishPreview();
				}

				_lastDrawnSelectedEntry.CleanUp();

				_lastDrawnSelectedEntry = null;
			}
		}

		public List<SearchyEntryBase> FilteredEntries
		{
			get
			{
				return SearchyCore.instance.FilteredEntries;
			}
		}

		public SearchyEntryBase SelectedEntry
		{
			get
			{
				return SearchyCore.instance.SelectedEntry;
			}

			set
			{
				SearchyCore.instance.SelectedEntry = value;
			}
		}

		private void SelectHome()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}

			SelectedEntry = FilteredEntries[0];
		}

		private void SelectEnd()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}

			SelectedEntry = FilteredEntries[FilteredEntries.Count-1];
		}

		private void SelectPageUp()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}

			int index = FilteredEntries.IndexOf(SelectedEntry);
			index-= 8;
			SelectedEntry = FilteredEntries[Mathf.Max(index, 0)];
		}

		private void SelectPageDown()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}
			int index = FilteredEntries.IndexOf(SelectedEntry);
			index+= 8;
			SelectedEntry = FilteredEntries[Mathf.Min(index, FilteredEntries.Count - 1)];
		}

		public void SelectNextEntry()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}
			int index = FilteredEntries.IndexOf(SelectedEntry);
			index++;
			SelectedEntry = FilteredEntries[index < FilteredEntries.Count ? index : 0];
		}

		public void SelectPreviousEntry()
		{
			if(FilteredEntries.Count == 0)
			{
				return;
			}
			int index = FilteredEntries.IndexOf(SelectedEntry);
			index--;
			SelectedEntry = FilteredEntries[index >= 0 ? index : FilteredEntries.Count - 1];
		}

		public void ScrollToSelectedEntry()
		{
			_scrollToSelectedOnNextFrame = true;
		}

		private void InternalScrollToSelectedEntry()
		{
			SearchyEntryBase cEntry = SelectedEntry;
			
			if(cEntry == null)
			{
				return;
			}

			float entryPanelHeight = position.height - (_padding + _searchFieldHeight + _padding);;
			float newTopY = cEntry.LastRect.y;

			float newBottomY = cEntry.LastRect.y - entryPanelHeight + Mathf.Min(cEntry.LastRect.height, entryPanelHeight);
			if(newBottomY >= SearchyCore.instance.CurrentSourceInChain.serializedScrollPos.y)
			{
				SearchyCore.instance.CurrentSourceInChain.serializedScrollPos = new Vector2(SearchyCore.instance.CurrentSourceInChain.serializedScrollPos.x, newBottomY);
			}
			else if(newTopY <= SearchyCore.instance.CurrentSourceInChain.serializedScrollPos.y)
			{
				SearchyCore.instance.CurrentSourceInChain.serializedScrollPos = new Vector2(SearchyCore.instance.CurrentSourceInChain.serializedScrollPos.x, newTopY);
			}
		}

		private void DrawSearchIcon(Rect inRect)
		{
			GUI.DrawTexture(inRect, SearchyWindowStyles.IconSearch);
		}

		private bool DrawSearchField(Rect inRect)
		{
			if(SelectedEntry != null && string.IsNullOrEmpty(SearchyCore.instance.SearchText))
			{
				GUI.Label(inRect, SelectedEntry.DisplayName, SearchyWindowStyles.SearchEmptyStyle);
			}
			else if(SelectedEntry != null)
			{
				string cLabel = SelectedEntry.SearchText;
				string cLabelLower = SelectedEntry.SearchText;
				string cSearchText = SearchyCore.instance.SearchText;
				string cSearchTextLower = SearchyCore.instance.SearchText.ToLower();
				string matchingString = "";
				bool matches = true;

				for(int i = 0; i < cSearchTextLower.Length; i++)
				{
					char cChar = cSearchTextLower[i];
					if(i >= cLabelLower.Length)
					{
						matches = false;
						break;
					}

					if(cChar != cLabelLower[i])
					{
						matches = false;
						break;
					}
					matchingString += cSearchText[i];
				}
				matchingString += cLabel.Substring(matchingString.Length);

				if(matches)
				{
					GUI.Label(inRect, matchingString, SearchyWindowStyles.SearchEmptyStyle);
				}
			}

			GUI.SetNextControlName("SearchySearchField");
			EditorGUI.BeginChangeCheck();
			SearchyCore.instance.SearchText = GUI.TextField(inRect, SearchyCore.instance.SearchText, SearchyWindowStyles.SearchStyle);

			if(GUI.GetNameOfFocusedControl() != "SearchySearchField")
			{
				GUI.FocusControl("SearchySearchField");
			}

			Event current = Event.current;
			if(current == Event.KeyboardEvent("Return") || current.character == '\n' || current.keyCode == KeyCode.Return || current.keyCode == KeyCode.KeypadEnter)
			{
				TextEditor txt = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
				txt.OnFocus();
				txt.SelectNone();
				txt.MoveTextEnd();
			}

			if(EditorGUI.EndChangeCheck())
			{
				return true;
			}

			return false;
		}

		private void DrawSelectedEntryIcon(Rect inRect)
		{
			if(SelectedEntry != null)
			{
				SelectedEntry.DrawIcon(inRect);
			}
		}

		private void DrawEntryPanel(Rect inRect)
		{
			Rect cRect = new Rect(inRect);

			Rect viewRect = DrawEntries(true, inRect.width);
			RefreshEntryRectsIfNeeded(viewRect.height > inRect.height ? inRect.width - 16 : inRect.width);
			SearchyCore.instance.CurrentSourceInChain.serializedScrollPos = GUI.BeginScrollView(cRect, SearchyCore.instance.CurrentSourceInChain.serializedScrollPos, viewRect, false, false);
			DrawEntries(false, inRect.width);
			GUI.EndScrollView();
		}

		private void RefreshEntryRectsIfNeeded(float inWidth)
		{
			if(!SearchyCore.instance.filteredEntryRectsNeedRefresh)
			{
				return;
			}
			
			SearchyCore.instance.filteredEntryRectsNeedRefresh = false;

			for(int i = 0; i < FilteredEntries.Count; i++)
			{
				FilteredEntries[i].LastRect = new Rect(0, i*_itemHeight, inWidth, _itemHeight);
			}
		}

		private List<SearchyEntryBase> _visibleEntries = new List<SearchyEntryBase>();
		private Rect DrawEntries(bool inOnlyCalculate, float inWidth)
		{
			Rect cRect = new Rect(0,0,inWidth,_itemHeight);

			float scrollPosY = SearchyCore.instance.CurrentSourceInChain.serializedScrollPos.y;

			if(inOnlyCalculate)
			{
				_visibleEntries.Clear();

				int firstVisibleIndex = Mathf.Max(0, Mathf.FloorToInt(scrollPosY/_itemHeight) - 10);
				int lastVisibleIndex = Mathf.Min(FilteredEntries.Count - 1, firstVisibleIndex + _itemMaxCount + 10);

				for(int i = firstVisibleIndex; i <= lastVisibleIndex; i++)
				{
					_visibleEntries.Add(FilteredEntries[i]);
				}
			}
			else if(Event.current.type == EventType.Repaint)
			{
				SearchyEntryBase selectedEntry = SelectedEntry;

				foreach(var cEntry in _visibleEntries)
				{
					cEntry.DrawEntry(cEntry.LastRect, cEntry == selectedEntry, 0);
				}
			}
			else if(Event.current.isMouse && Event.current.type == EventType.MouseDown)
			{
				foreach(var cEntry in _visibleEntries)
				{
					if(cEntry.LastRect.Contains(Event.current.mousePosition))
					{
						if(Event.current.clickCount >= 1 && SelectedEntry != cEntry)
						{
							SelectedEntry = cEntry;
						}
						else if(Event.current.clickCount == 2)
						{
							cEntry.ActivatePrimaryCommand();
						}
						else if(Event.current.button == 1)
						{
							cEntry.ActivateSecondaryCommand();
						}
					}
				}
			}

			return new Rect(0,0,cRect.width-16,FilteredEntries.Count*_itemHeight);
		}

		private void DrawPreviewPanel(Rect inRect)
		{
			GUILayout.BeginArea(inRect);
			_previewScrollPos = GUILayout.BeginScrollView(_previewScrollPos);
			inRect.x = 0;
			inRect.y = 0;
			SelectedEntry.DrawPreview(inRect);
			GUILayout.EndScrollView();
			GUILayout.EndArea();
		}

		[NonSerialized]
		private float _lastEntryPanelWidth = -1;
		[NonSerialized]
		private SearchyEntryBase _lastDrawnSelectedEntry = null;

		private void OnGUI()
		{
			if(SearchyCore.instance == null)
			{
				SearchyCore.Init();

				if(SearchyCore.instance == null)
				{
					Debug.LogError("Searchy: Unable to Init Core");
					CloseWindow();
					return;
				}
			}

			ProcessInput();

			if(_lastWindowSize == null)
			{
				_lastWindowSize = position;
			}

			if(_lastWindowSize != position)
			{
				WindowWidth = position.width;
				WindowHeight = position.height;
			}

			GUISkin origSkin = GUI.skin;
			GUI.skin = EditorGUIUtility.GetBuiltinSkin(EditorGUIUtility.isProSkin ? EditorSkin.Scene : EditorSkin.Inspector);
			GUI.skin.settings.cursorColor = new Color(1,1,1,.5f);
			GUI.skin.settings.cursorFlashSpeed = 1;
	
			SearchyCore.Init();

			Rect windowRect = new Rect(position);
			windowRect.x = 0;
			windowRect.y = 0;

			SearchyWindowUtils.DrawHorizontalSeparator(windowRect.y, windowRect.width);

			Rect paddedWindowRect = new Rect(windowRect);
			paddedWindowRect.x += _padding;
			paddedWindowRect.y += _padding;
			paddedWindowRect.width -= _padding*2;
			paddedWindowRect.height -= _padding*2;

			Rect searchIconRect = new Rect();
			searchIconRect.x = paddedWindowRect.x;
			searchIconRect.y = paddedWindowRect.y;
			searchIconRect.width = _searchFieldHeight;
			searchIconRect.height = _searchFieldHeight;
			DrawSearchIcon(searchIconRect);

			Rect searchFieldRect = new Rect();
			searchFieldRect.x = searchIconRect.x + searchIconRect.width + _padding;
			searchFieldRect.y = paddedWindowRect.y;
			searchFieldRect.width = paddedWindowRect.width - searchFieldRect.x - _selectedEntryIconSize;
			searchFieldRect.height = _searchFieldHeight;

			if(SearchyCore.instance.SourceChain.Count > 1)
			{
				for(int i = 1; i < SearchyCore.instance.SourceChain.Count; i++)
				{
					SearchyRuntimeSource cRuntimeSource = SearchyCore.instance.SourceChain[i];
					string cName = i == SearchyCore.instance.SourceChain.Count - 1 ? cRuntimeSource.SourceInstance.Name : (cRuntimeSource.SourceInstance.Name.Length > 8 ? cRuntimeSource.SourceInstance.Name.Substring(0, 8) + "..." : cRuntimeSource.SourceInstance.Name);

					GUIStyle nStyle = new GUIStyle(EditorStyles.boldLabel);
					nStyle.normal.textColor = Color.white;
					nStyle.alignment = TextAnchor.MiddleCenter;
					nStyle.fontStyle = FontStyle.Bold;
					
					Rect nRect = new Rect(searchFieldRect);
					nRect.width = nStyle.CalcSize(new GUIContent(cName)).x + 10;
					nRect.height = _searchFieldHeight;
					if(cRuntimeSource.SourceInstance.Icon != null)
					{
						nRect.width += nRect.height;
					}
					Color oldColor = GUI.color;
					GUI.color = Color.grey;
					GUI.DrawTexture(nRect, EditorGUIUtility.whiteTexture);
					GUI.color = oldColor;
					if(cRuntimeSource.SourceInstance.Icon != null)
					{
						nRect.x += nRect.height-6;
						nRect.width -= nRect.height-6;
					}
					GUI.Label(nRect, cName, nStyle);
					if(cRuntimeSource.SourceInstance.Icon != null)
					{
						Rect iconRect = new Rect(nRect);
						iconRect.x = searchFieldRect.x + 3;
						iconRect.y += 3;
						iconRect.height -= 6;
						iconRect.width = iconRect.height;
						GUI.DrawTexture(iconRect, cRuntimeSource.SourceInstance.Icon);
						nRect.width += nRect.height;
					}

					searchFieldRect.x += nRect.width + 10;
					searchFieldRect.width -= nRect.width + 10;
				}
			}

			DrawSearchField(searchFieldRect);

			if(_lastDrawnSelectedEntry != null && _lastDrawnSelectedEntry != SelectedEntry)
			{
				if(_lastDrawnSelectedEntry.HasPreview)
				{
					_lastDrawnSelectedEntry.FinishPreview();
				}

				_lastDrawnSelectedEntry.CleanUp();
			}

			_lastDrawnSelectedEntry = SelectedEntry;

			Rect selectedEntryIconRect = new Rect();
			selectedEntryIconRect.x = searchFieldRect.x + searchFieldRect.width + _padding;
			selectedEntryIconRect.y = paddedWindowRect.y;
			selectedEntryIconRect.width = _selectedEntryIconSize;
			selectedEntryIconRect.height = _selectedEntryIconSize;
			DrawSelectedEntryIcon(selectedEntryIconRect);

			float cPreviewPanelWidth = SelectedEntry != null && SelectedEntry.HasPreview ? PreviewPanelWidth : 0;

			Rect entryPanelRect = new Rect();
			entryPanelRect.x = windowRect.x;
			entryPanelRect.y = searchFieldRect.y + searchFieldRect.height + _padding;
			entryPanelRect.width = windowRect.width - cPreviewPanelWidth;
			entryPanelRect.height = windowRect.height - (_padding + _searchFieldHeight + _padding);
			SearchyWindowUtils.DrawHorizontalSeparator(entryPanelRect.y, windowRect.width);
			if(entryPanelRect.width != _lastEntryPanelWidth)
			{
				_lastEntryPanelWidth = entryPanelRect.width;
				SearchyCore.instance.filteredEntryRectsNeedRefresh = true;
			}

			if(cPreviewPanelWidth > 0)
			{
				Rect previewPanelRect = new Rect();
				previewPanelRect.x = windowRect.x + windowRect.width - cPreviewPanelWidth;
				previewPanelRect.y = searchFieldRect.y + searchFieldRect.height + _padding;
				previewPanelRect.width = cPreviewPanelWidth;
				previewPanelRect.height = windowRect.height - entryPanelRect.y;
		
				Rect resizeRect = new Rect(previewPanelRect.x - 5, previewPanelRect.y, 10, previewPanelRect.height);
				EditorGUIUtility.AddCursorRect(resizeRect, MouseCursor.ResizeHorizontal);
				SearchyWindowUtils.DrawVerticalSeparator(previewPanelRect.x, previewPanelRect.y, previewPanelRect.height);
				Event cEvent = Event.current;
				if(cEvent.type != EventType.Repaint)
				{
					if(cEvent.type == EventType.MouseDown && !_previewPanelResizing)
					{
						if(resizeRect.Contains(cEvent.mousePosition))
						{
							cEvent.Use();
							_previewPanelResizing = true;
						}
					}
					else if(_previewPanelResizing && cEvent.type == EventType.MouseDrag)
					{
						cEvent.Use();
						float tempPreviewPanelWidth = PreviewPanelWidth;
						tempPreviewPanelWidth -= Event.current.delta.x;
						tempPreviewPanelWidth = Mathf.Max(100, tempPreviewPanelWidth);
						tempPreviewPanelWidth = Mathf.Min(windowRect.width/2, tempPreviewPanelWidth);
						PreviewPanelWidth = tempPreviewPanelWidth;
					}
					else if(_previewPanelResizing && cEvent.type == EventType.MouseUp)
					{
						cEvent.Use();
						_previewPanelResizing = false;
					}
				}

				DrawPreviewPanel(previewPanelRect);
			}

			DrawEntryPanel(entryPanelRect);

			GUI.skin = origSkin;

			if(_scrollToSelectedOnNextFrame)
			{
				_scrollToSelectedOnNextFrame = false;

				InternalScrollToSelectedEntry();
			}
			
			// Always repaint so cursor animates
			Repaint();
		}

		private void UpdateWindowSize()
		{
			Rect nPos = new Rect(position);
			nPos.width = WindowWidth;
			nPos.height = WindowHeight;
		
			if(position.width != nPos.width || position.height != nPos.height)
			{
				position = nPos;
				Repaint();
			}

			minSize = new Vector2(600, _padding + _searchFieldHeight + _padding + _itemHeight * _itemMinCount);
			maxSize = new Vector2(2048, _padding + _searchFieldHeight + _padding + _itemHeight * _itemMaxCount);
		}
	}

}
