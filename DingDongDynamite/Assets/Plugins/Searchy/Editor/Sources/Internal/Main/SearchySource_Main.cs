using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	[Serializable]
	public class SearchySerializedSourceGroup
	{
		public string name;
		public bool collapsed = true;
		
		[NonSerialized]
		public SearchyEntry_SourceGroup runtimeEntry;
	}
	
	[Serializable]
	public class SearchySourceMainConfig : SearchyConfigBase
	{
		public string test = "Test";
		public List<SearchySerializedSourceGroup> sourceGroups = new List<SearchySerializedSourceGroup>();

		public SearchySerializedSourceGroup GetSourceGroupWithHash(string inHash)
		{
			foreach(var cGroup in sourceGroups)
			{
				if(cGroup.name == inHash)
				{
					return cGroup;
				}
			}
			
			return null;
		}

		public SearchySerializedSourceGroup CreateSourceGroup(SearchyRuntimeSource inRuntimeSource)
		{
			string cGroupName = inRuntimeSource.SourceInstance.GroupName;
			
			if(cGroupName == null)
			{
				return null;
			}

			foreach(var cGroup in sourceGroups)
			{
				if(cGroup.name == cGroupName)
				{
					return cGroup;
				}
			}

			var nGroup = new SearchySerializedSourceGroup() { name = cGroupName };
			sourceGroups.Add(nGroup);

			return nGroup;
		}
	} 

	public class SearchySource_Main : SearchySourceBase<SearchySourceMainConfig>
	{
		// TODO
		// Make these configurable in the config rather than hardcoded

		private List<Type> SubSourceTypes
		{
			get
			{
				return new List<Type>()
				{
					typeof(SearchySource_Favorites),
					typeof(SearchySource_Recents),
					typeof(SearchySource_Scenes),
					typeof(SearchySource_Scripts),
					typeof(SearchySource_HierarchyRoot),
					typeof(SearchySource_Prefabs),
					typeof(SearchySource_ScriptableObjects),
					typeof(SearchySource_Textures),
					typeof(SearchySource_Materials),
					typeof(SearchySource_Shaders),
					typeof(SearchySource_AudioClips),
					typeof(SearchySource_AudioMixers),
				};
			}
		}

		[NonSerialized]
		protected List<SearchyRuntimeSource> _runtimeSubSources = null;
		public List<SearchyRuntimeSource> RuntimeSubSources
		{
			get
			{
				if(_runtimeSubSources == null)
				{
					_runtimeSubSources = new List<SearchyRuntimeSource>();

					if(SubSourceTypes != null)
					{
						foreach(var cType in SubSourceTypes)
						{
							AddSubSourceOfType(cType);
						}
					}
				}

				return _runtimeSubSources;
			}
		}
		
		public SearchyRuntimeSource AddSubSourceOfType(Type inType)
		{
			SearchyRuntimeSource nRuntimeSource = new SearchyRuntimeSource(inType);
			_runtimeSubSources.Add(nRuntimeSource);
			return nRuntimeSource;
		}

		private List<SearchyEntryBase> _internalRefreshEntriesList = new List<SearchyEntryBase>();
		public override List<SearchyEntryBase> InternalRefreshEntries(SearchyGetEntryMode inMode)
		{
			_internalRefreshEntriesList.Clear();

			if(SubSourceTypes != null)
			{
				foreach(var cRuntimeSource in RuntimeSubSources)
				{
					if(!cRuntimeSource.enabled)
					{
						continue;
					}
					
					SearchyEntry_Source nEntry = new SearchyEntry_Source(cRuntimeSource);
					if(inMode == SearchyGetEntryMode.NoSearch)
					{
						SearchySerializedSourceGroup cGroup = Config.CreateSourceGroup(cRuntimeSource);
						if(cGroup != null)
						{
							if(cGroup.runtimeEntry == null)
							{
								cGroup.runtimeEntry = new SearchyEntry_SourceGroup() { parentSource = this, Hash = cGroup.name, SubIconTexture = nEntry.Icon };
							}
							
							if(!_internalRefreshEntriesList.Contains(cGroup.runtimeEntry))
							{
								_internalRefreshEntriesList.Add(cGroup.runtimeEntry);
							}

							if(!cGroup.collapsed)
							{
								_internalRefreshEntriesList.Add(nEntry);
							}
						}
						else //if(cRuntimeSource.SourceInstance.InternalRefreshEntries(SearchyGetEntryMode.NoSearchAsSubSource).Count > 0)
						{
							_internalRefreshEntriesList.Add(nEntry);
						}
					}

					if(inMode == SearchyGetEntryMode.Search || inMode == SearchyGetEntryMode.SearchAsSubSource)
					{
						_internalRefreshEntriesList.AddRange(nEntry.SourceInstance.InternalRefreshEntries(SearchyGetEntryMode.SearchAsSubSource));
					}
				}
			}

			_internalRefreshEntriesList.AddRange(RefreshEntries(inMode));
			return _internalRefreshEntriesList;
		}
	}
}
