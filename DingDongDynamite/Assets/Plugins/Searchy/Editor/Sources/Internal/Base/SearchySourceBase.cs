using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public enum SearchyGetEntryMode
	{
		NoSearch,
		Search,
		NoSearchAsSubSource,
		SearchAsSubSource
	}

	[Serializable]
	public class SearchySerializedEntry
	{
		[SerializeField]
		private string _createdTimeString;
		private DateTime? _createdTime = null;
		public DateTime CreatedTime
		{
			get
			{
				if(_createdTime == null)
				{
					_createdTime = System.Convert.ToDateTime(_createdTimeString);
				}

				return _createdTime.Value;
			}
		}
		public string entryHash;
		public string entryTypeFullName;

		public SearchySerializedEntry(string inHash, string inTypeFullName)
		{
			_createdTimeString = DateTime.UtcNow.ToString();
			entryHash = inHash;
			entryTypeFullName = inTypeFullName;
		}

		public SearchySerializedEntry(SearchyEntryBase inEntry)
		{
			_createdTimeString = DateTime.UtcNow.ToString();
			entryHash = inEntry.Hash;
			entryTypeFullName = inEntry.GetType().FullName;
		}

		public SearchyEntryBase CreateEntry(SearchySourceBase inParentSource)
		{
			SearchyCore.SearchyEntryType entryType = null;
			foreach(var cEntryType in SearchyCore.instance.EntryTypes)
			{
				if(cEntryType.typeFullName == entryTypeFullName)
				{
					entryType = cEntryType;
					break;
				}
			}

			if(entryType == null)
			{
				return null;
			}

			SearchyEntryBase nEntryInstance = Activator.CreateInstance(entryType.type) as SearchyEntryBase;
			if(nEntryInstance == null)
			{
				return nEntryInstance;
			}
			
			nEntryInstance.Hash = entryHash;
			nEntryInstance.parentSource = inParentSource;
			return nEntryInstance;
		}
	}

	[Serializable]
	public class SearchyConfigBase
	{
		public string searchyVersion;
		public List<SearchySerializedEntry> favorites = new List<SearchySerializedEntry>();
		public List<SearchySerializedEntry> recents = new List<SearchySerializedEntry>();
	}

	public abstract class SearchySourceBaseDefaultConfig : SearchySourceBase<SearchyConfigBase>
	{
	}

	public abstract class SearchySourceBase<TConfig> : SearchySourceBase where TConfig : SearchyConfigBase, new()
	{
		private TConfig _config;
		public TConfig Config
		{
			get
			{
				if(_config == null)
				{
					InternalLoadConfig();
				}
				return _config;
			}
		}

		protected virtual TConfig DefaultConfig()
		{
			TConfig nConfig = new TConfig();
			return nConfig;
		}

		public override void InternalSaveConfig()
		{
			if(_config == null)
			{
				_config = DefaultConfig();
			}

			_config.searchyVersion = SearchyCore.Version;

			string json = JsonUtility.ToJson(_config);
			EditorPrefs.SetString(EditorPrefsKey, json);
		}

		public override void InternalLoadConfig()
		{
			if(EditorPrefs.HasKey(EditorPrefsKey))
			{
				_config = JsonUtility.FromJson<TConfig>(EditorPrefs.GetString(EditorPrefsKey));
				if(_config == null)
				{
					_config = DefaultConfig();
				}
			}
			else
			{
				_config = DefaultConfig();
			}
		}

		public override bool MatchRecent(SearchyEntryBase inEntry)
		{
			foreach(var cRecent in Config.recents)
			{
				if(cRecent.entryHash == inEntry.Hash)
				{
					return true;
				}
			}

			return false;
		}

		public override void AddRecent(SearchyEntryBase inEntry)
		{
			SearchySerializedEntry nSerializedEntry = new SearchySerializedEntry(inEntry);
			SearchySerializedEntry existingSerializedEntry = null;
			
			foreach(var cRecent in _config.recents)
			{
				if(cRecent.entryTypeFullName == nSerializedEntry.entryTypeFullName && cRecent.entryHash == nSerializedEntry.entryHash)
				{
					existingSerializedEntry = cRecent;
					break;
				}
			}

			if(existingSerializedEntry != null)
			{
				_config.recents.Remove(existingSerializedEntry);
			}

			_config.recents.Insert(0, nSerializedEntry);

			if(_config.recents.Count > 5)
			{
				while(_config.recents.Count > 5)
				{
					_config.recents.RemoveAt(_config.recents.Count-1);
				}
			}

			InternalSaveConfig();
			SearchyCore.instance.MarkAllNeedsFullRefresh(typeof(SearchySource_Recents));
		}

		public override bool MatchFavorite(SearchyEntryBase inEntry)
		{
			foreach(var cFavorite in Config.favorites)
			{
				if(cFavorite.entryHash == inEntry.Hash)
				{
					return true;
				}
			}

			return false;
		}

		public override void ToggleFavorite(SearchyEntryBase inEntry)
		{
			SearchySerializedEntry nSerializedEntry = new SearchySerializedEntry(inEntry);
			SearchySerializedEntry existingSerializedEntry = null;
			
			foreach(var cFavorite in _config.favorites)
			{
				if(cFavorite.entryTypeFullName == nSerializedEntry.entryTypeFullName && cFavorite.entryHash == nSerializedEntry.entryHash)
				{
					existingSerializedEntry = cFavorite;
					break;
				}
			}

			if(existingSerializedEntry != null)
			{
				_config.favorites.Remove(existingSerializedEntry);
			}
			else
			{
				_config.favorites.Add(nSerializedEntry);
			}

			InternalSaveConfig();
			SearchyCore.instance.MarkAllNeedsFullRefresh(this.GetType());
			SearchyCore.instance.MarkAllNeedsFullRefresh(typeof(SearchySource_Favorites));
		}
	}

	public abstract class SearchySourceBase
	{
		private bool _inited = false;
		public SearchyRuntimeSource boundRuntimeSource = null;

		public virtual bool RefreshOnProjectChange
		{
			get
			{
				return false;
			}
		}

		public virtual bool RefreshOnHierarchyChange
		{
			get
			{
				return false;
			}
		}

		public virtual Texture2D Icon
		{
			get
			{
				return null;
			}
		}

		public virtual string GroupName
		{
			get
			{
				return null;
			}
		}

		public virtual string Name
		{
			get
			{
				return "Untitled";
			}
		}

		public virtual bool IsVisible
		{
			get
			{
				return true;
			}
		}

		protected class SearchyEntriesForMode
		{
			public bool needsRefresh = true;
			public List<SearchyEntryBase> list = new List<SearchyEntryBase>();
		}

		public SearchyEntryBase FindEntryWithHash(string inHash)
		{
			foreach(var cMode in Enum.GetValues(typeof(SearchyGetEntryMode)))
			{
				List<SearchyEntryBase> entries = InternalGetEntries((SearchyGetEntryMode)cMode);
				foreach(var cEntry in entries)
				{
					if(cEntry.Hash == inHash)
					{
						return cEntry;
					}
				}
			}

			return null;
		}

		[NonSerialized]
		protected Dictionary<SearchyGetEntryMode, SearchyEntriesForMode> _entries = new Dictionary<SearchyGetEntryMode, SearchyEntriesForMode>
		{
			{ SearchyGetEntryMode.NoSearch, new SearchyEntriesForMode() },
			{ SearchyGetEntryMode.Search, new SearchyEntriesForMode() },
			{ SearchyGetEntryMode.NoSearchAsSubSource, new SearchyEntriesForMode() },
			{ SearchyGetEntryMode.SearchAsSubSource, new SearchyEntriesForMode() },
		};

		public List<SearchyEntryBase> InternalGetEntries(SearchyGetEntryMode inMode)
		{
			SearchyEntriesForMode entriesForMode = _entries[inMode];

			if(entriesForMode.needsRefresh)
			{
				entriesForMode.needsRefresh = false;
				entriesForMode.list.Clear();
				entriesForMode.list.AddRange(InternalRefreshEntries(inMode));
			}

			return entriesForMode.list;
		}
		
		public virtual List<SearchyEntryBase> InternalRefreshEntries(SearchyGetEntryMode inMode)
		{
			return RefreshEntries(inMode);
		}

		protected List<SearchyEntryBase> _refreshEntriesList = new List<SearchyEntryBase>();
		public virtual List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();
			return _refreshEntriesList;
		}
		
		public void MarkNeedsFullRefresh()
		{
			foreach(var cEntriesForMode in _entries)
			{
				cEntriesForMode.Value.needsRefresh = true;
			}
		}

		public void InternalInit()
		{
			if(_inited)
			{
				return;
			}
			
			_inited = true;
			InternalLoadConfig();
			Init();
		}

		public virtual void Init()
		{
		}

		public virtual void SortEntries(List<SearchyEntryBase> inList)
		{
			int topIndex = 0;

			for(int i = 0; i < inList.Count; i++)
			{
				if(inList[i].SubLabel == null)
				{
					topIndex++;
				}
				else if(inList[i].SubLabel != null && inList[i].SubLabel == "/")
				{
					inList.Insert(topIndex, inList[i]);
					inList.RemoveAt(i+1);
					topIndex++;
				}
			}
		}

		public virtual bool MatchRecent(SearchyEntryBase inEntry)
		{
			return false;
		}

		public virtual void AddRecent(SearchyEntryBase inEntry)
		{
		}

		public virtual bool MatchFavorite(SearchyEntryBase inEntry)
		{
			return false;
		}

		public virtual void ToggleFavorite(SearchyEntryBase inEntry)
		{
		}

		public virtual void InternalSaveConfig()
		{
		}

		public virtual void InternalLoadConfig()
		{
		}

		private string _editorPrefsKey;
		protected string EditorPrefsKey
		{
			get
			{
				if(_editorPrefsKey == null)
				{
					_editorPrefsKey = "Searchy_" + this.GetType().FullName;
				}

				return _editorPrefsKey;
			}
		}
	}
}
