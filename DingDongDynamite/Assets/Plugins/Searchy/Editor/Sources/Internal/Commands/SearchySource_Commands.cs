using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Commands : SearchySourceBaseDefaultConfig
	{
		[NonSerialized]
		private bool _parentEntryCreated;
		[NonSerialized]
		private SearchyEntryBase _parentEntry;
		public SearchyEntryBase ParentEntry
		{
			get
			{
				if(!_parentEntryCreated)
				{
					_parentEntryCreated = true;
					string entryHash = boundRuntimeSource.GetOptionalData("CommandEntryHash", null);
					string entryTypeFullName = boundRuntimeSource.GetOptionalData("CommandEntryTypeFullName", null);
					string parentSourceTypeFullName = boundRuntimeSource.GetOptionalData("CommandEntryParentSourceTypeFullName", null);
				
					SearchySourceBase parentSource = SearchyCore.instance.GetSourceInstanceWithTypeName(parentSourceTypeFullName);

					if(parentSource == null)
					{
						return _parentEntry;
					}

					SearchySerializedEntry nEntryHashSet = new SearchySerializedEntry(entryHash, entryTypeFullName);
					_parentEntry = nEntryHashSet.CreateEntry(parentSource);
				}

				return _parentEntry;
			}
		}

		public override string Name
		{
			get
			{
				if(ParentEntry == null)
				{
					return "Commands";
				}
				
				return ParentEntry.DisplayName;
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return ParentEntry.Icon;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();

			if(ParentEntry == null)
			{
				return _refreshEntriesList;
			}

			ParentEntry.InternalRefreshCommands();

			foreach(SearchyCommand cCommand in ParentEntry.CommandList)
			{
				_refreshEntriesList.Add(new SearchyEntry_Command() { parentCommand = cCommand });
			}

			return _refreshEntriesList;
		}
	}
}
