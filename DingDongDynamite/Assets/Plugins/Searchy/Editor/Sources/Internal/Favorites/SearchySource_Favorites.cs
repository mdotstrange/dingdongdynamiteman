using UnityEngine;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Favorites : SearchySourceBaseDefaultConfig
	{
		public override string Name
		{
			get
			{
				return "Favorites";
			}
		}

		public override bool IsVisible
		{
			get
			{
				return InternalGetEntries(SearchyGetEntryMode.NoSearch).Count > 0;
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIcon("Favorite Icon");
			}
		}

		public override bool RefreshOnHierarchyChange
		{
			get
			{
				return true;
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();

			switch(inMode)
			{
				case SearchyGetEntryMode.NoSearchAsSubSource:
				case SearchyGetEntryMode.SearchAsSubSource:
				{
					return _refreshEntriesList;
				}
			}

			RecurseFavorites(_refreshEntriesList, SearchyCore.instance.MainSourceInChain.SourceInstance);

			return _refreshEntriesList;
		}

		private void RecurseFavorites(List<SearchyEntryBase> inOutList, SearchySourceBase inSource)
		{
			SearchySourceBaseDefaultConfig inSourceAsDefaultConfig = inSource as SearchySourceBaseDefaultConfig;
			SearchySource_Main inSourceAsBaseGroup = inSource as SearchySource_Main;

			if(inSourceAsDefaultConfig == null && inSourceAsBaseGroup == null)
			{
				return;
			}

			foreach(var cFavorite in inSourceAsDefaultConfig != null ? inSourceAsDefaultConfig.Config.favorites : inSourceAsBaseGroup.Config.favorites)
			{
				inOutList.Add(cFavorite.CreateEntry(inSourceAsDefaultConfig));
			}

			if(inSourceAsBaseGroup == null)
			{
				return;
			}

			foreach(var cSubSource in inSourceAsBaseGroup.RuntimeSubSources)
			{
				RecurseFavorites(inOutList, cSubSource.SourceInstance);
			}
		}
	}
}
