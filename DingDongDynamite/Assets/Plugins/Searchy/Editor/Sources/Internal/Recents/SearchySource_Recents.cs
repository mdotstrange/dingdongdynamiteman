using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Recents : SearchySourceBaseDefaultConfig
	{
		public override string Name
		{
			get
			{
				return "Recents";
			}
		}

		public override bool IsVisible
		{
			get
			{
				return InternalGetEntries(SearchyGetEntryMode.NoSearch).Count > 0;
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowStyles.IconRecents;
			}
		}

		public override bool RefreshOnHierarchyChange
		{
			get
			{
				return true;
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();
			
			switch(inMode)
			{
				case SearchyGetEntryMode.NoSearchAsSubSource:
				case SearchyGetEntryMode.SearchAsSubSource:
				{
					return _refreshEntriesList;
				}
			}

			List<SearchySortableRecent> sortableRecents = new List<SearchySortableRecent>();
			RecurseRecents(sortableRecents, SearchyCore.instance.MainSourceInChain.SourceInstance);
			sortableRecents.Sort((x, y) => DateTime.Compare(y.serializedEntry.CreatedTime, x.serializedEntry.CreatedTime));
			foreach(var cSortableRecent in sortableRecents)
			{
				_refreshEntriesList.Add(cSortableRecent.entry);
			}
			return _refreshEntriesList;
		}

		private class SearchySortableRecent
		{
			public SearchySerializedEntry serializedEntry;
			public SearchyEntryBase entry;
		}

		private void RecurseRecents(List<SearchySortableRecent> inOutList, SearchySourceBase inSource)
		{
			SearchySourceBaseDefaultConfig inSourceAsDefaultConfig = inSource as SearchySourceBaseDefaultConfig;
			SearchySource_Main inSourceAsBaseGroup = inSource as SearchySource_Main;

			if(inSourceAsDefaultConfig == null && inSourceAsBaseGroup == null)
			{
				return;
			}

			foreach(var cRecent in inSourceAsDefaultConfig != null ? inSourceAsDefaultConfig.Config.recents : inSourceAsBaseGroup.Config.recents)
			{
				SearchySortableRecent nSortableRecent = new SearchySortableRecent();
				nSortableRecent.serializedEntry = cRecent;
				nSortableRecent.entry = cRecent.CreateEntry(inSourceAsDefaultConfig);
				if(!nSortableRecent.entry.IsValid)
				{
					continue;
				}
				inOutList.Add(nSortableRecent);
			}

			if(inSourceAsBaseGroup != null)
			{
				foreach(var cSubSource in inSourceAsBaseGroup.RuntimeSubSources)
				{
					RecurseRecents(inOutList, cSubSource.SourceInstance);
				}
			}
		}
	}
}
