using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_AudioClips : SearchySourceBaseAsset
	{
		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("wav");
			}
		}

		public override string GroupName
		{
			get
			{
				return "Audio";
			}
		}

		public override string Name
		{
			get
			{
				return "AudioClips";
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_AudioClip>("t:audioclip");
			return _refreshEntriesList;
		}
	}
}
