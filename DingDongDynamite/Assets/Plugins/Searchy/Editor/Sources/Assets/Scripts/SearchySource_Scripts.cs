using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Scripts : SearchySourceBaseAsset
	{
		public override string Name
		{
			get
			{
				return "Scripts";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("cs");
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			// Skip DLLs and things in Packages
			RefreshFromAssetDBSearch<SearchyEntry_Script>("t:script", null, new string[] { ".dll", "es/com" } );
			
			// Sort "Plugins" last
			int topIndex = 0;
			for(int i = 0; i < _refreshEntriesList.Count; i++)
			{
				SearchyEntry_Script cEntry = _refreshEntriesList[i] as SearchyEntry_Script;

				if(!cEntry.AssetPath.Contains("Plugins"))
				{
					_refreshEntriesList.Insert(topIndex, cEntry);
					_refreshEntriesList.RemoveAt(i+1);
					topIndex++;
				}
			}

			return _refreshEntriesList;
		}
	}
}
