using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Scenes : SearchySourceBaseAsset
	{
		public override string Name
		{
			get
			{
				return "Scenes";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("unity");
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_Scene>("t:scene", null);
			return _refreshEntriesList;
		}
	}
}
