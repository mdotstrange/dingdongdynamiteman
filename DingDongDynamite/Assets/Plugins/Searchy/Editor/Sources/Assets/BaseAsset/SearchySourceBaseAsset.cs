using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FlyingWorm.Searchy
{
	public abstract class SearchySourceBaseAsset : SearchySourceBaseDefaultConfig
	{
		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		protected void RefreshFromAssetDBSearch<T>(string inSearch, string[] inRequiredContains = null, string[] inSkipWhenContains = null) where T : SearchyEntryBaseAsset, new()
		{
			_refreshEntriesList.Clear();

			string[] guids = AssetDatabase.FindAssets(inSearch);
			for(int i = 0; i < guids.Length; i++)
			{
				string cPath = AssetDatabase.GUIDToAssetPath(guids[i]);
				
				bool cIsValid = true;
				if(inRequiredContains != null)
				{
					foreach(var cRequiredContains in inRequiredContains)
					{
						if(!cPath.Contains(cRequiredContains))
						{
							cIsValid = false;
							break;
						}
					}

					if(!cIsValid)
					{
						continue;
					}
				}

				if(inSkipWhenContains != null)
				{
					foreach(var cSkipWhenContains in inSkipWhenContains)
					{
						if(cPath.Contains(cSkipWhenContains))
						{
							cIsValid = false;
							break;
						}
					}

					if(!cIsValid)
					{
						continue;
					}
				}

				T nEntry = new T();
				nEntry.guid = guids[i];
				nEntry.parentSource = this;
				_refreshEntriesList.Add(nEntry);
			}

			int topIndex = 0;
			for(int i = 0; i < _refreshEntriesList.Count; i++)
			{
				SearchyEntryBaseAsset cEntry = _refreshEntriesList[i] as SearchyEntryBaseAsset;
				if(cEntry == null || cEntry.AssetPath == null)
				{
					_refreshEntriesList.RemoveAt(i);
					i--;
					continue;
				}
				
				if(cEntry.AssetPathFolder == "/")
				{
					_refreshEntriesList.Insert(topIndex, cEntry);
					_refreshEntriesList.RemoveAt(i+1);
					topIndex++;
				}
			}
		}
	}
}
