using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Textures : SearchySourceBaseAsset
	{
		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("png");
			}
		}

		public override string GroupName
		{
			get
			{
				return "Art";
			}
		}

		public override string Name
		{
			get
			{
				return "Textures";
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_Texture>("t:texture", null, new string[] { ".ttf", ".otf" } );
			return _refreshEntriesList;
		}
	}
}
