using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Shaders : SearchySourceBaseAsset
	{
		public override string GroupName
		{
			get
			{
				return "Art";
			}
		}

		public override string Name
		{
			get
			{
				return "Shaders";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("shader");
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_Shader>("t:shader");
			return _refreshEntriesList;
		}
	}
}
