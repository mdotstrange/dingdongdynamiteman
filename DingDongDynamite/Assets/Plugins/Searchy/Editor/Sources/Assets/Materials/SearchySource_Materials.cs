using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Materials : SearchySourceBaseAsset
	{
		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("mat");
			}
		}

		public override string GroupName
		{
			get
			{
				return "Art";
			}
		}

		public override string Name
		{
			get
			{
				return "Materials";
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_Material>("t:material", new string[] { ".mat" });
			return _refreshEntriesList;
		}
	}
}
