using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Prefabs : SearchySourceBaseAsset
	{
		public override string GroupName
		{
			get
			{
				return "Assets";
			}
		}

		public override string Name
		{
			get
			{
				return "Prefabs";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("prefab");
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_Prefab>("t:prefab");
			return _refreshEntriesList;
		}
	}
}
