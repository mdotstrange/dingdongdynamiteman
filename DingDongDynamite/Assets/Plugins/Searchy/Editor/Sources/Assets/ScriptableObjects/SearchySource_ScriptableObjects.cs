using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_ScriptableObjects : SearchySourceBaseAsset
	{
		public override string GroupName
		{
			get
			{
				return "Assets";
			}
		}

		public override string Name
		{
			get
			{
				return "ScriptableObjects";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("asset");
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_ScriptableObject>("t:ScriptableObject");
			return _refreshEntriesList;
		}
	}
}
