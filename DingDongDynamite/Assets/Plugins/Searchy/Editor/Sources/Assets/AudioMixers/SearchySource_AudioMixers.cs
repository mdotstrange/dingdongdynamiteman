using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_AudioMixers : SearchySourceBaseAsset
	{
		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowUtils.LoadEditorIconWithExtension("mixer");
			}
		}

		public override string GroupName
		{
			get
			{
				return "Audio";
			}
		}

		public override string Name
		{
			get
			{
				return "AudioMixers";
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			RefreshFromAssetDBSearch<SearchyEntry_AudioMixer>("t:audiomixer");
			return _refreshEntriesList;
		}
	}
}
