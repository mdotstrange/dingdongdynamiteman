using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_Hierarchy : SearchySourceBaseDefaultConfig
	{
		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowStyles.IconHierarchy;
			}
		}

		private int InstanceID
		{
			get
			{
				return boundRuntimeSource.GetOptionalData("FilterInstanceID", -1);
			}
		}

		private GameObject _objectInstance = null;
		private GameObject ObjectInstance
		{
			get
			{
				if(_objectInstance == null)
				{
					_objectInstance = EditorUtility.InstanceIDToObject(InstanceID) as GameObject;
				}

				return _objectInstance;
			}
		}

		private string _name = null;
		public override string Name
		{
			get
			{
				if(String.IsNullOrEmpty(_name) && ObjectInstance != null)
				{
					_name = ObjectInstance.name;
				}

				return _name;
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override bool RefreshOnHierarchyChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();

			if(ObjectInstance == null)
			{
				return _refreshEntriesList;
			}

			Transform cTrans = ObjectInstance.transform;


			switch(inMode)
			{
				case SearchyGetEntryMode.NoSearch:
				{
					foreach(Transform cChild in cTrans)
					{
						SearchyEntry_Hierarchy nEntry = new SearchyEntry_Hierarchy() { instanceID = cChild.gameObject.GetInstanceID(), parentSource = this };
						_refreshEntriesList.Add(nEntry);
					}

					break;
				}
				
				case SearchyGetEntryMode.Search:
				case SearchyGetEntryMode.SearchAsSubSource:
				{
					Transform[] children = cTrans.GetComponentsInChildren<Transform>(true);
					foreach(Transform cChild in children)
					{
						if(cChild == cTrans)
						{
							continue;
						}
						
						SearchyEntry_Hierarchy nEntry = new SearchyEntry_Hierarchy() { instanceID = cChild.gameObject.GetInstanceID(), parentSource = this };
						_refreshEntriesList.Add(nEntry);
					}

					break;
				}
			}
			
			return _refreshEntriesList;
		}
	}
}
