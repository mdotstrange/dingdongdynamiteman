using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace FlyingWorm.Searchy
{
	public class SearchySource_HierarchyRoot : SearchySourceBaseDefaultConfig
	{
		public override string Name
		{
			get
			{
				return "Hierarchy";
			}
		}

		public override Texture2D Icon
		{
			get
			{
				return SearchyWindowStyles.IconHierarchy;
			}
		}

		public override bool RefreshOnProjectChange
		{
			get
			{
				return true;
			}
		}

		public override bool RefreshOnHierarchyChange
		{
			get
			{
				return true;
			}
		}

		public override List<SearchyEntryBase> RefreshEntries(SearchyGetEntryMode inMode)
		{
			_refreshEntriesList.Clear();

			List<Transform> rootTransforms = new List<Transform>();
			List<string> rootCategories = new List<string>();

			HierarchyProperty hierarchyProp = new HierarchyProperty(HierarchyType.GameObjects);
			hierarchyProp.Reset();
			List<int> rootSceneIDs = new List<int>();
			while(hierarchyProp.Next(new int[0]))
			{
				rootSceneIDs.Add(hierarchyProp.instanceID);
			}

			switch(inMode)
			{
				case SearchyGetEntryMode.NoSearch:
				{
					while(hierarchyProp.Next(rootSceneIDs.ToArray()))
					{
						GameObject cObj = hierarchyProp.pptrValue as GameObject;
						if(cObj == null)
						{
							continue;
						}

						SearchyEntry_Hierarchy nEntry = new SearchyEntry_Hierarchy() { instanceID = cObj.GetInstanceID(), parentSource = this };
						_refreshEntriesList.Add(nEntry);
					}
					break;
				}

				case SearchyGetEntryMode.Search:
				case SearchyGetEntryMode.SearchAsSubSource:
				{
					while(hierarchyProp.Next(rootSceneIDs.ToArray()))
					{
						GameObject cObj = hierarchyProp.pptrValue as GameObject;
						if(cObj == null)
						{
							continue;
						}

						rootTransforms.Add(cObj.transform);
						rootCategories.Add(hierarchyProp.GetScene().name);
					}

					for(int i = 0; i < rootTransforms.Count; i++)
					{
						Transform cTrans = rootTransforms[i];

						Transform[] children = cTrans.GetComponentsInChildren<Transform>(true);
						foreach(Transform cChild in children)
						{
							SearchyEntry_Hierarchy nEntry = new SearchyEntry_Hierarchy() { instanceID = cChild.gameObject.GetInstanceID(), parentSource = this };
							_refreshEntriesList.Add(nEntry);
						}
					}
					break;
				}
			}

			return _refreshEntriesList;
		}
	}
}
